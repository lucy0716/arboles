/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.controladores;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.modelo.ArbolN;
import com.arboles.modelo.Infante;
import com.arboles.modelo.Localidad;
import com.arboles.modelo.NodoABB;
import com.arboles.modelo.NodoN;
import com.arboles.utilidades.FacesUtils;
import com.arboles.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author Lucia Valencia
 */
@Named(value = "controladorArbolN")
@SessionScoped
public class ControladorArbolN implements Serializable {

    private ArbolN arbolN = new ArbolN();
    private Infante infante = new Infante();

    private ControladorABB contAbb = (ControladorABB) FacesUtils.getManagedBean("controladorABB");

    private DefaultDiagramModel model;
    private String identificacionPadre;
    private String identificacionPadre2;
    private boolean verRegistrar;
    private List<Localidad> localidades;

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public ControladorArbolN() {
    }

    public Infante getInfante() {
        return infante;
    }

    public String getInfanteSeleccionado() {
        return infanteSeleccionado;
    }

    public void setInfanteSeleccionado(String infanteSeleccionado) {
        this.infanteSeleccionado = infanteSeleccionado;
    }

    public String getIdentificacionPadre2() {
        return identificacionPadre2;
    }

    public void setIdentificacionPadre2(String identificacionPadre2) {
        this.identificacionPadre2 = identificacionPadre2;
    }

    public void setInfante(Infante infante) {
        this.infante = infante;
    }

    public String getIdentificacionPadre() {
        return identificacionPadre;
    }

    public void setIdentificacionPadre(String identificacionPadre) {
        this.identificacionPadre = identificacionPadre;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public void habilitarVerRegistrar() {
        verRegistrar = !verRegistrar;
    }

    @PostConstruct
    public void inicializar() {
        localidades = new ArrayList<>();
        localidades.add(new Localidad("16917", "Caldas"));
        localidades.add(new Localidad("16017001", "Manizales"));
        localidades.add(new Localidad("05", "Antioquia"));
        localidades.add(new Localidad("16005001", "Medellín"));

//        try {
//            arbolN.adicionarN(new Infante("1", (byte) 3, "Lucia", new Localidad("123", "manizales")), null);
//            arbolN.adicionarN(new Infante("2", (byte) 2, "carlos", new Localidad("123", "manizales")), "1");
//            arbolN.adicionarN(new Infante("3", (byte) 1, "valeria", new Localidad("123", "manizales")), "1");
//            arbolN.adicionarN(new Infante("4", (byte) 5, "sofia", new Localidad("123", "manizales")), "2");
//            arbolN.adicionarN(new Infante("5", (byte) 2, "Salome", new Localidad("123", "manizales")), "1");
//
//        } catch (InfanteExcepcion ex) {
//            JsfUtil.addErrorMessage(ex.getMessage());
//            //Logger.getLogger(ControladorABB.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        pintarArbol();
    }

    //metodo Guuardar infante
    public void guardarInfante() {
        try {
            arbolN.adicionarN(infante, identificacionPadre);
            infante = new Infante();
            verRegistrar = false;
            pintarArbol();
        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    // metodo diagramar arbol
    public DiagramModel getModel() {
        return model;
    }

    public void pintarArbol() {

        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbolN.getRaiz(), model, null, 30, 0);

    }

    private void pintarArbol(NodoN reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            //elementHijo.setId(reco.getDato().getNroIdentificacion());

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);
            for (NodoN hijo : reco.getHijos()) {
                pintarArbol(hijo, model, elementHijo, x - 10, y + 5);
                x += 10;
            }
        }
    }

    //click derecchoo
    public void onClickRight() {
        String id = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("elementId");

        infanteSeleccionado = id.replaceAll("frmInfantesAbb:diagrama-", "");

    }

    private String infanteSeleccionado = " ";

    public void eliminarInfanteMenuContext() throws InfanteExcepcion {

//        if (arbolN.eliminarNodo(infanteSeleccionado)!= null) {
//            pintarArbol();
//            JsfUtil.addSuccessMessage("Eliminado con éxito");
//        } else {
//            JsfUtil.addErrorMessage("No pudo eliminarse, comuníquese con el administrador ");
//        }
    }

//    // metodo de cargar arbol en un solo padre
//    public void cargarArbol() {
//        try {
//            //Algoritmo
//            arbolN.adicionarNodo(new Infante("0", (byte) 1, "Yo", null), null);
//            ControladorABB contAbb = (ControladorABB) FacesUtils.
//                    getManagedBean("controladorABB");
//
//            Infante pivote = new Infante();
//            pivote = arbolN.getRaiz().getDato();
//
//            for (Infante inf : contAbb.getArbol().recorrerPreOrden()) {
//
//                for (int i = 0; i < inf.getEdad(); i++) {
//
//                    arbolN.adicionarNodo(inf, pivote);
//
//                }
//                pivote = inf;
//            }
//        } catch (InfanteExcepcion ex) {
//            JsfUtil.addErrorMessage(ex.getMessage());
//        }
//
//        pintarArbol();
//    }
    //metodo de cargar arbol repartido en padres 
    public void cargarArbol1() {
        try {
            //Algoritmo
            arbolN.adicionarNodo(new Infante("0", (byte) 1, "Yo", null), null);

            List<Infante> listaHijos = new ArrayList<>();
            List<Infante> listaPadres = new ArrayList<>();
            Infante padre = new Infante();
            padre = arbolN.getRaiz().getDato();
            int contInf = 0;
            int contPadre = 0;
            for (Infante inf : contAbb.getArbol().recorrerPreOrden()) {
                for (int i = 0; i < padre.getEdad(); i++) {
                    listaPadres.add(padre);
                }
                for (int i = 0; i < inf.getEdad(); i++) {
                    listaHijos.add(inf);
                }

                for (int i = 0; i < listaHijos.size(); i++) {
                    if (listaHijos.size() > listaPadres.size()) {
                        if (contPadre < listaPadres.size()) {
                            arbolN.adicionarNodoN(listaHijos.get(i), listaPadres.get(contPadre), contPadre);
                            contPadre++;
                        } else {
                            contPadre = 0;
                            arbolN.adicionarNodoN(listaHijos.get(i), listaPadres.get(contPadre), contPadre);
                            contPadre++;
                        }
                    } else if (listaHijos.size() <= listaPadres.size()) {
                        arbolN.adicionarNodoN(inf, padre, i);
                    }
                }

                padre = inf;
                listaHijos.clear();
                listaPadres.clear();
            }

        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }

        pintarArbol();
    }

    // intercambiar padres
    public void intercambiar() {
        try {
            arbolN.intercambiarHijos(identificacionPadre, identificacionPadre2);

        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        pintarArbol();
    }

    //Metodo cargar arbol profeee
    public void cargarArbol() {
        try {
            //Algoritmo
            arbolN = new  ArbolN();
            arbolN.adicionarNodo(new Infante("0", (byte) 0,
                    "Yo", null), null);

            List<Infante> padres = new ArrayList<Infante>();
            padres.add(arbolN.getRaiz().getDato());
            adicionarPreOrden(contAbb.getArbol().getRaiz(), padres, 0);

            System.out.println("Cantidad: " + contAbb.getArbol().
                    getCantidadNodos());

        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        pintarArbol();
    }

    // adiccionar preorden
    private void adicionarPreOrden(NodoABB reco, List<Infante> padres, int contizq) throws InfanteExcepcion {

        if (reco != null) {

            List<Infante> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getEdad(); i++) {

                Infante infanteNuevo = new Infante(
                        reco.getDato().getNroIdentificacion(), reco.getDato().getEdad(),
                        reco.getDato().getNombre(), reco.getDato().getLocalidad());
                infanteNuevo.setCodigo(++contizq);

                if (contPapas >= padres.size()) {

                    contPapas = 0;
                }
                arbolN.adicionarNodoxCodigo(infanteNuevo, padres.get(contPapas));
                padresNuevos.add(infanteNuevo);
                contPapas++;
            }
            adicionarPreOrden(reco.getIzquierda(), padresNuevos, contizq);
            contizq = contizq + contAbb.getArbol().sumarEdades(reco.getIzquierda());
            adicionarPreOrden(reco.getDerecha(), padresNuevos, contizq);
        }
        pintarArbol();

    }

}
