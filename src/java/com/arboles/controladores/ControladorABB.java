/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.controladores;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.modelo.ArbolBinarioB;
import com.arboles.modelo.Infante;
import com.arboles.modelo.Localidad;
import com.arboles.modelo.NodoABB;
import com.arboles.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author carloaiza
 */
@Named(value = "controladorABB")
@SessionScoped
public class ControladorABB implements Serializable {

    private ArbolBinarioB arbol = new ArbolBinarioB();
    private Infante infante = new Infante();
    private List<Localidad> localidades;

    private boolean verRegistrar;
    private boolean mostrarReco;
    private boolean mostrarInfantesLetra;
    private boolean mostrarLista;
    private boolean mostrarInfantesXLetra;
    private boolean mostrarPadreInfantes;

    private DefaultDiagramModel model;
//    private NodoABB menorEdad;
    private byte menorEdad;
    private float promedioEdades;
    private int sumaEdades;
    private byte edad;
    private String letra;
    private String buscarPadre;
    private String borrarInfanteIdentificacion;

    /**
     * Creates a new instance of ControladorABB
     */
    public ControladorABB() {
    }

    public boolean isMostrarPadreInfantes() {
        return mostrarPadreInfantes;
    }

    public void setMostrarPadreInfantes(boolean mostrarPadreInfantes) {
        this.mostrarPadreInfantes = mostrarPadreInfantes;
    }

    public boolean isMostrarInfantesXLetra() {
        return mostrarInfantesXLetra;
    }

    public void setMostrarInfantesXLetra(boolean mostrarInfantesXLetra) {
        this.mostrarInfantesXLetra = mostrarInfantesXLetra;
    }

    public boolean isMostrarLista() {
        return mostrarLista;
    }

    public void setMostrarLista(boolean mostrarLista) {
        this.mostrarLista = mostrarLista;
    }

    public boolean isMostrarInfantesLetra() {
        return mostrarInfantesLetra;
    }

    public void setMostrarInfantesLetra(boolean mostrarInfantesLetra) {
        this.mostrarInfantesLetra = mostrarInfantesLetra;
    }

    public String getBorrarInfanteIdentificacion() {
        return borrarInfanteIdentificacion;
    }

    public void setBorrarInfanteIdentificacion(String borrarInfanteIdentificacion) {
        this.borrarInfanteIdentificacion = borrarInfanteIdentificacion;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public String getBuscarPadre() {
        return buscarPadre;
    }

    public void setBuscarPadre(String buscarPadre) {
        this.buscarPadre = buscarPadre;
    }

    public boolean isMostrarReco() {
        return mostrarReco;
    }

    public void setMostrarReco(boolean mostrarReco) {
        this.mostrarReco = mostrarReco;
    }

    public byte getEdad() {
        return edad;
    }

    public void setEdad(byte edad) {
        this.edad = edad;
    }

    public int getSumaEdades() {
        return sumaEdades;
    }

    public void setSumaEdades(int sumaEdades) {
        this.sumaEdades = sumaEdades;
    }

    public float getPromedioEdades() {
        return promedioEdades;
    }

    public void setPromedioEdades(float promedioEdades) {
        this.promedioEdades = promedioEdades;
    }

    public byte getMenorEdad() {
        return menorEdad;
    }

    public void setMenorEdad(byte menorEdad) {
        this.menorEdad = menorEdad;
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    @PostConstruct
    public void inicializar() {
        localidades = new ArrayList<>();
        localidades.add(new Localidad("16917", "Caldas"));
        localidades.add(new Localidad("16017001", "Manizales"));
        localidades.add(new Localidad("05", "Antioquia"));
        localidades.add(new Localidad("16005001", "Medellín"));

        try {
            arbol.adicionarNodo(new Infante("3", (byte) 1,
                    "Santiaguito", localidades.get(1)));
            arbol.adicionarNodo(new Infante("2", (byte) 3,
                    "Sebitas", localidades.get(1)));
            arbol.adicionarNodo(new Infante("8", (byte) 2,
                    "Mateito", localidades.get(1)));
            arbol.adicionarNodo(new Infante("6", (byte) 3,
                    "Lucy", localidades.get(1)));
            arbol.adicionarNodo(new Infante("5", (byte) 2,
                    "Diiego", localidades.get(1)));
            arbol.adicionarNodo(new Infante("7", (byte) 2,
                    "Josee", localidades.get(1)));
            arbol.adicionarNodo(new Infante("4", (byte) 3,
                    "Kareen", localidades.get(1)));

        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
            //Logger.getLogger(ControladorABB.class.getName()).log(Level.SEVERE, null, ex);
        }
        verRegistrar = false;
        pintarArbol();
    }

    public ArbolBinarioB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolBinarioB arbol) {
        this.arbol = arbol;
    }

    public Infante getInfante() {
        return infante;
    }

    public void setInfante(Infante infante) {
        this.infante = infante;
    }

    public void habilitarVerRegistrar() {
        verRegistrar = true;
    }

    public void pintarArbol() {

        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);

    }

    private void pintarArbol(NodoABB reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            elementHijo.setId(reco.getDato().getNroIdentificacion());

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbol(reco.getIzquierda(), model, elementHijo, x - 5, y + 5);
            pintarArbol(reco.getDerecha(), model, elementHijo, x + 5, y + 5);
        }
    }

    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DiagramModel getModel() {
        return model;
    }

    //metodo Guuardar nuevo dato
    public void guardarInfante() {
        try {
            arbol.adicionarNodo(infante);
            infante = new Infante();
            verRegistrar = false;
            pintarArbol();
        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    //Buscar infante menor
    public void buscarMenor() throws InfanteExcepcion {
        arbol.buscarMenor();
        menorEdad = arbol.buscarMenor();
    }

    //Sumar las edades de los infantes
    public void sumarEdades() {
        arbol.sumarEdades();
        sumaEdades = arbol.sumarEdades();
    }

    //Calcular el promedio de las edades
    public void calcularPromedioEdades() {
        arbol.calcularPromedioEdades();
        promedioEdades = arbol.calcularPromedioEdades();
    }

    // metodo bsucar edades iguales 
    public void edadesIguales() {
        arbol.buscarEdadIguales(edad);
    }

    // mostar los recorridos del arbol
    public void habilitarTablas() {
        mostrarReco = !mostrarReco;
    }

    //Cancelar Guardar 
    public void cancelarGuardar() {
        verRegistrar = false;
    }

    // Metodo eliminar infante 
    public void eliminarInfante() {
        arbol.borrarInfante(borrarInfanteIdentificacion);
        pintarArbol();
    }

    // mostrar tabla de listar intantes 
    public void listarInfantes() {
        mostrarInfantesLetra = !mostrarInfantesLetra;
    }

    // podar arbol= eliminar las hojas 
    public void podarArbol() {
        arbol.borarHojas();
        pintarArbol();
    }

    // se elimina el mayor de los infantes
    public void borrarMayor() {
        arbol.borrarInfanteMayor();
        pintarArbol();
    }

    //se elimina el menor de los infantes 
    public void borrarMenor() {
        arbol.borrarInfanteMenor();
        pintarArbol();
    }

    //Se habilita el boton para motrar las la tabla de las hojas 
    public void habilitarlistarHojas() {
        mostrarLista = !mostrarLista;
    }

    //Se habilita el boton para motrar la tabla infante por letras
    public void habilitarListarXLetra() {
        mostrarInfantesXLetra = !mostrarInfantesXLetra;
    }

    //Se habilita el boton para motrar la tabla deñ padre del infante 
    public void habilitarPadre() {
        mostrarPadreInfantes = !mostrarPadreInfantes;
    }

    //Cambiar  valores de las identificaciones de los infantes 
    public void cambiarValores() {
        arbol.cambiarValores();
        pintarArbol();
    }

    // ON CLICK DERECHO
    public void onClickRight() {
        String id = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("elementId");

        infanteSeleccionado = id.replaceAll("frmInfantesAbb:diagrama-", "");

    }
    private String infanteSeleccionado = "";

    // ON CLICK DERECHO eliminar infaante
    public void eliminarInfanteMenuContext() {

        if (arbol.borrarInfante(infanteSeleccionado) != null) {
            pintarArbol();
            JsfUtil.addSuccessMessage("Eliminado con éxito");
        } else {
            JsfUtil.addErrorMessage("No pudo eliminarse, comuníquese con el administrador ");
        }
    }
}
