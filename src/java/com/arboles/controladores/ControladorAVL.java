/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.controladores;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.modelo.ArbolAVL;
import com.arboles.modelo.Infante;
import com.arboles.modelo.Localidad;
import com.arboles.modelo.NodoAVL;
import com.arboles.utilidades.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author Lucia Valencia
 */
@Named(value = "controladorAVL")
@SessionScoped
public class ControladorAVL implements Serializable {

    private ArbolAVL arbol = new ArbolAVL();
    private Infante infante = new Infante();
    private Infante dato;
    private List<Localidad> localidades;

    private boolean verRegistrar;
    private boolean mostrarReco;
    private boolean mostrarInfantesLetra;
    private boolean mostrarLista;
    private boolean mostrarInfantesXLetra;
    private boolean mostrarPadreInfantes;
    private boolean mostrarAltura;
    private boolean mostrarNiveles;

    private DefaultDiagramModel model;
//    private NodoABB menorEdad;
    private byte menorEdad;
    private float promedioEdades;
    private int sumaEdades;
    private byte edad;
    private String letra;
    private String buscarPadre;
    private String borrarInfanteIdentificacion;

    /**
     * Creates a new instance of ControladorABB
     */
    public ControladorAVL() {
    }

    public boolean isMostrarNiveles() {
        return mostrarNiveles;
    }

    public void setMostrarNiveles(boolean mostrarNiveles) {
        this.mostrarNiveles = mostrarNiveles;
    }

    public boolean isMostrarAltura() {
        return mostrarAltura;
    }

    public void setMostrarAltura(boolean mostrarAltura) {
        this.mostrarAltura = mostrarAltura;
    }

    public Infante getDato() {
        return dato;
    }

    public void setDato(Infante dato) {
        this.dato = dato;
    }

    public boolean isMostrarPadreInfantes() {
        return mostrarPadreInfantes;
    }

    public void setMostrarPadreInfantes(boolean mostrarPadreInfantes) {
        this.mostrarPadreInfantes = mostrarPadreInfantes;
    }

    public boolean isMostrarInfantesXLetra() {
        return mostrarInfantesXLetra;
    }

    public void setMostrarInfantesXLetra(boolean mostrarInfantesXLetra) {
        this.mostrarInfantesXLetra = mostrarInfantesXLetra;
    }

    public boolean isMostrarLista() {
        return mostrarLista;
    }

    public void setMostrarLista(boolean mostrarLista) {
        this.mostrarLista = mostrarLista;
    }

    public boolean isMostrarInfantesLetra() {
        return mostrarInfantesLetra;
    }

    public void setMostrarInfantesLetra(boolean mostrarInfantesLetra) {
        this.mostrarInfantesLetra = mostrarInfantesLetra;
    }

    public String getBorrarInfanteIdentificacion() {
        return borrarInfanteIdentificacion;
    }

    public void setBorrarInfanteIdentificacion(String borrarInfanteIdentificacion) {
        this.borrarInfanteIdentificacion = borrarInfanteIdentificacion;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public String getBuscarPadre() {
        return buscarPadre;
    }

    public void setBuscarPadre(String buscarPadre) {
        this.buscarPadre = buscarPadre;
    }

    public boolean isMostrarReco() {
        return mostrarReco;
    }

    public void setMostrarReco(boolean mostrarReco) {
        this.mostrarReco = mostrarReco;
    }

    public byte getEdad() {
        return edad;
    }

    public void setEdad(byte edad) {
        this.edad = edad;
    }

    public int getSumaEdades() {
        return sumaEdades;
    }

    public void setSumaEdades(int sumaEdades) {
        this.sumaEdades = sumaEdades;
    }

    public float getPromedioEdades() {
        return promedioEdades;
    }

    public void setPromedioEdades(float promedioEdades) {
        this.promedioEdades = promedioEdades;
    }

    public byte getMenorEdad() {
        return menorEdad;
    }

    public void setMenorEdad(byte menorEdad) {
        this.menorEdad = menorEdad;
    }

    public List<Localidad> getLocalidades() {
        return localidades;
    }

    public void setLocalidades(List<Localidad> localidades) {
        this.localidades = localidades;
    }

    public boolean isVerRegistrar() {
        return verRegistrar;
    }

    public void setVerRegistrar(boolean verRegistrar) {
        this.verRegistrar = verRegistrar;
    }

    public ArbolAVL getArbol() {
        return arbol;
    }

    public void setArbol(ArbolAVL arbol) {
        this.arbol = arbol;
    }

    public Infante getInfante() {
        return infante;
    }

    public void setInfante(Infante infante) {
        this.infante = infante;
    }

    public void habilitarVerRegistrar() {
        verRegistrar = true;
    }

    @PostConstruct
    public void inicializar() {
        localidades = new ArrayList<>();
        localidades.add(new Localidad("16917", "Caldas"));
        localidades.add(new Localidad("16017001", "Manizales"));
        localidades.add(new Localidad("05", "Antioquia"));
        localidades.add(new Localidad("16005001", "Medellín"));

        try {
            arbol.adicionarNodo(new Infante("1", (byte) 3, "Santiaguito", localidades.get(1)));
            arbol.adicionarNodo(new Infante("2", (byte) 2, "mateito", localidades.get(1)));
            arbol.adicionarNodo(new Infante("6", (byte) 4, "Sebitas", localidades.get(1)));
            arbol.adicionarNodo(new Infante("7", (byte) 2, "Cami", localidades.get(1)));
            arbol.adicionarNodo(new Infante("8", (byte) 3, "Laura", localidades.get(1)));

        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
            //Logger.getLogger(ControladorABB.class.getName()).log(Level.SEVERE, null, ex);
        }
        verRegistrar = false;
        pintarArbol();
    }

    public void pintarArbol() {

        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);

    }

    private void pintarArbol(NodoAVL reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco);

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            elementHijo.setStyleClass("ui-diagram-element-busc");
            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbol(reco.getIzquierda(), model, elementHijo, x - 5, y + 5);
            pintarArbol(reco.getDerecha(), model, elementHijo, x + 5, y + 5);
        }
    }

    public DiagramModel getModel() {
        return model;
    }

    //metodo Guuardar nuevo dato
    public void guardarInfante() {
        try {
            arbol.adicionarNodo(infante);
            JsfUtil.addSuccessMessage("El dato ha sido adicionado");
            infante = new Infante();
            verRegistrar = false;
            pintarArbol();
        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    //Buscar infante menor
    public void buscarMenor() throws InfanteExcepcion {
        arbol.buscarMenor();
        menorEdad = arbol.buscarMenor();
    }

    //Sumar las edades de los infantes
    public void sumarEdades() {
        arbol.sumarEdades();
        sumaEdades = arbol.sumarEdades();
    }

    //Calcular el promedio de las edades
    public void calcularPromedioEdades() {
        arbol.calcularPromedioEdades();
        promedioEdades = arbol.calcularPromedioEdades();
    }

    // metodo bsucar edades iguales 
    public void edadesIguales() {
        arbol.buscarEdadIguales(edad);
    }

    // mostar los recorridos del arbol
    public void habilitarTablas() {
        mostrarReco = !mostrarReco;
    }

    //Cancelar Guardar 
    public void cancelarGuardar() {
        verRegistrar = false;
    }

    // Metodo eliminar infante 
    public void eliminarInfante() {
        arbol.borrarInfante(borrarInfanteIdentificacion);
        pintarArbol();
    }

    // mostrar tabla de listar intantes 
    public void listarInfantes() {
        mostrarInfantesLetra = !mostrarInfantesLetra;
    }

    // podar arbol= eliminar las hojas 
    public void podarArbol() {
        arbol.borarHojas();
        pintarArbol();
    }

    // se elimina el mayor de los infantes
    public void borrarMayor() {
        arbol.borrarInfanteMayor();
        pintarArbol();
    }

    //se elimina el menor de los infantes 
    public void borrarMenor() {
        arbol.borrarInfanteMenor();
        pintarArbol();
    }

    //Se habilita el boton para motrar las la tabla de las hojas 
    public void habilitarlistarHojas() {
        mostrarLista = !mostrarLista;
    }

    //Se habilita el boton para motrar la tabla infante por letras
    public void habilitarListarXLetra() {
        mostrarInfantesXLetra = !mostrarInfantesXLetra;
    }

    //Se habilita el boton para motrar la tabla deñ padre del infante 
    public void habilitarPadre() {
        mostrarPadreInfantes = !mostrarPadreInfantes;
    }

    //Cambiar  valores de las identificaciones de los infantes 
    public void cambiarValores() {
        arbol.cambiarValores();
        pintarArbol();
    }

    // ON CLICK DERECHO
    public void onClickRight() {
        String id = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap().get("elementId");

        infanteSeleccionado = id.replaceAll("frmInfantesAbb:diagrama-", "");

    }
    private String infanteSeleccionado = "";

    // ON CLICK DERECHO eliminar infaante
    public void eliminarInfanteMenuContext() {

        if (arbol.borrarInfante(infanteSeleccionado) != null) {
            pintarArbol();
            JsfUtil.addSuccessMessage("Eliminado con éxito");
        } else {
            JsfUtil.addErrorMessage("No pudo eliminarse, comuníquese con el administrador ");
        }
    }

    // balancear arbol
    public void balancear() {
        arbol.balancear_x(arbol.getRaiz());
        pintarArbol();
    }

    // altura del aarbol
    public void alturaArbol() {
        try {
            arbol.isLleno();
            arbol.retornarAltura();
            pintarArbol();

        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    // niveles de arboles
    public void niveles() {
        try {
            arbol.isLleno();
            arbol.impNiveles();
            pintarArbol();
        } catch (InfanteExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    //Se habilita el boton para motrar la altura del arbol
    public void habilitarAltura() {
        mostrarAltura = !mostrarAltura;
    }
    //Se habilita el boton para motrar la altura del arbol
    public void habilitarNiveles() {
        mostrarNiveles = !mostrarNiveles;
    }
}
