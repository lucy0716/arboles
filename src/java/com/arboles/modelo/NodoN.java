/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucia Valencia
 */
public class NodoN implements Serializable {

    private Infante dato;
    private List<NodoN> hijos;

    public NodoN(Infante dato) {
        this.dato = dato;
        hijos = new ArrayList<>();
    }

    public Infante getDato() {
        return dato;
    }

    public void setDato(Infante dato) {
        this.dato = dato;
    }

    public List<NodoN> getHijos() {
        return hijos;
    }

    public void setHijos(List<NodoN> hijos) {
        this.hijos = hijos;
    }

    @Override
    public String toString() {
        return "NodoN{" + "dato=" + dato + ", hijos=" + hijos + '}';
    }
    
    public int obtenerNumeroHijos(){
        return hijos.size();
    }
            
    public void aumentarHijo(NodoN hijo){
        hijos.add(hijo);
    }
    

}
