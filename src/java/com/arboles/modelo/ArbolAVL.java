/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.modelo;

import com.arboles.excepciones.InfanteExcepcion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucia Valencia
 */
public class ArbolAVL {

    private NodoAVL raiz;
    private int cantidadNodos;

    public NodoAVL getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoAVL raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    // saber si esta vacio o no 
    public void isLleno() throws InfanteExcepcion {
        if (raiz == null) {
            throw new InfanteExcepcion("El árbol está vacío");
        }
    }

    // adiccionar nodo al arbol
    public void adicionarNodo(Infante dato) throws InfanteExcepcion {
//        InfanteValidador.verificarDatosObligatorios(dato);
//        InfanteValidador.verificarEdadInfante(dato.getEdad());
        NodoAVL nodo = new NodoAVL(dato);
        if (raiz == null) {
            raiz = nodo;
            cantidadNodos++;
        } else {
            adicionarNodo(nodo, raiz);
            cantidadNodos++;
        }
    }

    public void adicionarNodo(NodoAVL nuevo, NodoAVL pivote) throws InfanteExcepcion {

        if (nuevo.getDato().getNroIdentificacion().compareTo(pivote.getDato().getNroIdentificacion()) == 0) {
            throw new InfanteExcepcion("Ya existe un infante con la identificación " + nuevo.getDato().getNroIdentificacion());
        } else if (nuevo.getDato().getNroIdentificacion().compareTo(pivote.getDato().getNroIdentificacion()) < 0) {
            ///Va para la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //va para la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }

        pivote.actualizarAltura();
        balancear_x(pivote);
    }

    //Balancear arbol avl
    public void balancear_x(NodoAVL principal) {
         if (!principal.esVacio()) {
            int fe = principal.obtenerFactorEquilibrio();
            boolean signo = true;
            if (fe < 0) {
                signo = false;
                fe = fe * -1;
            }
            if (fe >= 2) {
                //Esta desequilibrado
                //hacia donde
                if (signo) {
                    //Desequilibrio a la derecha
                    //Valido desequilibrio simple a la izq
                    if (principal.getDerecha().obtenerFactorEquilibrio() > 0) {
                        //Desequilibrio simple - Rotacion simple
                        rotarSimpleNuevo(principal, signo);

                    } else {
                        //Desequilibrio doble - Rotación doble
                        rotarSimpleNuevo(principal.getDerecha(), false);
                        rotarSimpleNuevo(principal, true);
                    }
                } else {
                    //Desequilibrio a la izquierda
                    //Valido desequilibrio simple a la izq
                    if (principal.getIzquierda().obtenerFactorEquilibrio() < 0) {
                        rotarSimpleNuevo(principal, signo);
                    } else {
                        //Tengo un zig zag
                        //rotar doble
                        rotarSimpleNuevo(principal.getIzquierda(), true);
                        rotarSimpleNuevo(principal, false);
                    }
                }
            }
        }

    }

    //Rotar arbol para asi poder balancearlo
    public void rotarSimpleNuevo(NodoAVL princ, boolean sentido) {

        if (!sentido) {
            if (princ.getDerecha() != null) {
                NodoAVL nodoN = princ.getDerecha();
                princ.setDerecha(new NodoAVL(princ.getDato()));
                princ.getDerecha().setDerecha(nodoN);
            } else {
                princ.setDerecha(new NodoAVL(princ.getDato()));
            }
            princ.setDato(princ.getIzquierda().getDato());
            if (princ.getIzquierda().getDerecha() != null) {
                NodoAVL nodoN = princ.getIzquierda().getDerecha();
                princ.setIzquierda(princ.getIzquierda().getIzquierda());
                princ.getIzquierda().setDerecha(nodoN);
            } else {
                princ.setIzquierda(princ.getIzquierda().getIzquierda());
            }
            princ.getDerecha().actualizarAltura();
        } else {
            if (princ.getIzquierda() != null) {
                NodoAVL nodoN = princ.getIzquierda();
                princ.setIzquierda(new NodoAVL(princ.getDato()));
                princ.getIzquierda().setIzquierda(nodoN);
            } else {
                princ.setIzquierda(new NodoAVL(princ.getDato()));
            }
            princ.setDato(princ.getDerecha().getDato());
            if (princ.getDerecha().getIzquierda() != null) {
                NodoAVL nodoN = princ.getDerecha().getIzquierda();
                princ.setDerecha(princ.getDerecha().getDerecha());
                princ.getDerecha().setIzquierda(nodoN);
            } else {
                princ.setDerecha(princ.getDerecha().getDerecha());
            }
            princ.getIzquierda().actualizarAltura();
        }
        princ.actualizarAltura();
    }

  

    

       

    public List<Infante> recorrerInOrden() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        List<Infante> listaInfantes = new ArrayList<>();
        recorrerInOrden(raiz, listaInfantes);
        return listaInfantes;
    }

    private void recorrerInOrden(NodoAVL reco, List<Infante> listaInfantes) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listaInfantes);
            listaInfantes.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listaInfantes);
        }
    }

    public List<Infante> recorrerPreOrden() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        List<Infante> listaInfantes = new ArrayList<>();
        recorrerPreOrden(raiz, listaInfantes);
        return listaInfantes;
    }

    private void recorrerPreOrden(NodoAVL reco, List<Infante> listaInfantes) {
        if (reco != null) {
            listaInfantes.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(), listaInfantes);
            recorrerPreOrden(reco.getDerecha(), listaInfantes);
        }
    }

    public List<Infante> recorrerPostOrden() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        List<Infante> listaInfantes = new ArrayList<>();
        recorrerPostOrden(raiz, listaInfantes);
        return listaInfantes;
    }

    private void recorrerPostOrden(NodoAVL reco, List<Infante> listaInfantes) {
        if (reco != null) {

            recorrerPostOrden(reco.getIzquierda(), listaInfantes);
            recorrerPostOrden(reco.getDerecha(), listaInfantes);
            listaInfantes.add(reco.getDato());
        }
    }

    public boolean esVacio() {
        return raiz == null;
    }

    //  Buscar menor Metodo parcial con coleeciones
    public byte buscarMenor() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        byte menor = 0;
        ArrayList listaInfantes = new ArrayList();
        buscarMenor(raiz, listaInfantes);
        menor = (byte) Collections.min(listaInfantes);

        return menor;
    }

    private void buscarMenor(NodoAVL reco, ArrayList listaInfantes) {
        if (reco != null) {
            listaInfantes.add(reco.getDato().getEdad());
            buscarMenor(reco.getIzquierda(), listaInfantes);
            buscarMenor(reco.getDerecha(), listaInfantes);

        }
    }

    //buscar nodo MENOR por metodo recursivo
    public Infante buscarInfanteMenor() {
        return buscarInfanteMenor(raiz);
    }

    private Infante buscarInfanteMenor(NodoAVL ubicacion) {
        for (; ubicacion.getIzquierda() != null; ubicacion = ubicacion.getIzquierda());
        return (ubicacion.getDato());
    }

    //buscar nodo MAYOR metodo recursivo
    public Infante buscarInfanteMayor() {
        return buscarInfanteMayor(raiz);
    }

    private Infante buscarInfanteMayor(NodoAVL pivote) {
        for (; pivote.getDerecha() != null; pivote = pivote.getDerecha());
        return (pivote.getDato());
    }

    // Sumar edades metodo normal
    private int sumaEdadesArbol;

    public int sumarEdadesArbol() {
        sumaEdadesArbol = 0;
        sumarInOrden(raiz);
        return sumaEdadesArbol;
    }

    private void sumarInOrden(NodoAVL reco) {
        if (reco != null) {
            sumarInOrden(reco.getIzquierda());
            sumaEdadesArbol += reco.getDato().getEdad();
            sumarInOrden(reco.getDerecha());
        }

    }

    // Suma edades metodo recursivo
    public int sumarEdades() {
        return sumarEdades(raiz);
    }

    public int sumarEdades(NodoAVL reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarEdades(reco.getIzquierda());
            suma += reco.getDato().getEdad();
            suma += sumarEdades(reco.getDerecha());
        }
        return suma;
    }

    public float calcularPromedioEdades() {
        return sumarEdades() / (float) cantidadNodos;
    }

    // Contar infantes que tengan la misma edad 
    public int contarInfantexEdad(byte edad) {
        return contarInfantexEdad(raiz, edad);
    }

    private int contarInfantexEdad(NodoAVL reco, byte edad) {
        int cont = 0;
        if (reco != null) {
            if (reco.getDato().getEdad() == edad) {
                cont++;
            }
            cont += contarInfantexEdad(reco.getIzquierda(), edad);
            cont += contarInfantexEdad(reco.getIzquierda(), edad);
        }

        return cont;
    }

    // buscar infantes con edades iguales
    public int buscarEdadIguales(byte edad) {

        return buscarEdadIguales(raiz, edad);
    }

    private int buscarEdadIguales(NodoAVL reco, byte edad) {
        int cont = 0;
        if (reco != null) {
            cont += buscarEdadIguales(reco.getIzquierda(), edad);
            if (edad == reco.getDato().getEdad()) {
                cont++;
            }
            cont += buscarEdadIguales(reco.getDerecha(), edad);
        }
        return cont;
    }

    // Listar hojas del arbol
    public List<Infante> listarHojas() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        List<Infante> listaInfantes = new ArrayList<>();
        listarHojas(raiz, listaInfantes);

        return listaInfantes;
    }

    private void listarHojas(NodoAVL reco, List<Infante> listaInfantes) {
        if (reco != null) {
            if (reco.getIzquierda() == null && reco.getDerecha() == null) {
                listaInfantes.add(reco.getDato());
            }
            listarHojas(reco.getIzquierda(), listaInfantes);
            listarHojas(reco.getDerecha(), listaInfantes);
        }
    }

    // Buscar infante por letra
    public List<Infante> buscarInfantexLetra(String letra) {
        List<Infante> lista = new ArrayList<>();
        buscarInfantexLetra(raiz, letra, lista);
        return lista;

    }

    private void buscarInfantexLetra(NodoAVL reco, String letra, List<Infante> lista) {
        if (reco != null) {
            if (reco.getDato().getNombre().toUpperCase().contains(letra.toUpperCase())) {
                lista.add(reco.getDato());
            }
            buscarInfantexLetra(reco.getIzquierda(), letra, lista);
            buscarInfantexLetra(reco.getDerecha(), letra, lista);

        }
    }

    // Buscar padre de un infante
    public List<Infante> buscarPadre(String identificacio) {
        List<Infante> lista = new ArrayList<>();
        return buscarPadre(raiz, identificacio, lista);
    }

    private List<Infante> buscarPadre(NodoAVL reco, String identificacio, List<Infante> lista) {
        if (reco != null) {
            if (reco.getIzquierda() != null && reco.getIzquierda().getDato().getNroIdentificacion().equals(identificacio) || reco.getDerecha() != null && reco.getDerecha().getDato().getNroIdentificacion().equals(identificacio)) {
                lista.add(reco.getDato());
            }
            buscarPadre(reco.getIzquierda(), identificacio, lista);
            buscarPadre(reco.getDerecha(), identificacio, lista);
        }

        return lista;
    }

    //Metodo Borrar el que sea
    private NodoAVL buscarNodoMenor(NodoAVL pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
    }

    public String borrarInfante(String identificacion) {

        NodoAVL z = borrarinfante(this.raiz, identificacion);
        this.setRaiz(z);
        return identificacion;
    }

    private NodoAVL borrarinfante(NodoAVL pivote, String identificacion) {
        if (pivote == null) {
            return null;//<--Dato no encontrado		
        }
        int compara = pivote.getDato().getNroIdentificacion().compareTo(identificacion);
        if (compara > 0) {
            pivote.setIzquierda(borrarinfante(pivote.getIzquierda(), identificacion));
        } else if (compara < 0) {
            pivote.setDerecha(borrarinfante(pivote.getDerecha(), identificacion));
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoAVL cambiar = buscarNodoMenor(pivote.getDerecha());
                Infante aux = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(aux);
                pivote.setDerecha(borrarinfante(pivote.getDerecha(), identificacion));
            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda() : pivote.getDerecha();
            }
        }
        return pivote;
    }

    //metodo Podar arbol
    public void borarHojas() {
        borrarHojas(raiz);
    }

    private void borrarHojas(NodoAVL pivote) {
        if (pivote != null) {
            if ((pivote.getIzquierda() == null && pivote.getDerecha() == null)) {
                borrarInfante(pivote.getDato().getNroIdentificacion());
            }
            borrarHojas(pivote.getIzquierda());
            borrarHojas(pivote.getDerecha());
        }
    }

    //Borrar infante de maayor edad
    public void borrarInfanteMayor() {
        borrarInfanteMayor(raiz);
    }

    private void borrarInfanteMayor(NodoAVL reco) {
        if (reco != null) {
            if (reco.getDato() == buscarInfanteMayor()) {
                borrarInfante(reco.getDato().getNroIdentificacion());
            }
            borrarInfanteMayor(reco.getDerecha());
        }

    }

    //metodo borrar infante de menor edad
    public void borrarInfanteMenor() {
        borrarInfanteMenor(raiz);
    }

    private void borrarInfanteMenor(NodoAVL reco) {
        if (reco != null) {
            if (reco.getDato() == buscarInfanteMenor()) {
                borrarInfante(reco.getDato().getNroIdentificacion());
            }
            borrarInfanteMenor(reco.getIzquierda());
            borrarInfanteMenor(reco.getDerecha());
        }

    }

    // metodo de cambiar valores a los nodos
    public boolean cambiarValores() {
        cambiarValores(raiz);
        return true;
    }

    private void cambiarValores(NodoAVL reco) {
        if (reco != null) {
            int valor = Integer.parseInt(reco.getDato().getNroIdentificacion());
            valor = valor * 3;
            String nuevoValor = String.valueOf(valor);
            reco.getDato().setNroIdentificacion(nuevoValor);
            cambiarValores(reco.getIzquierda());
            cambiarValores(reco.getDerecha());
        }
    }

    // metodo obtener rama mayor
    int numeroRamas = 0;

    public ArrayList<String> obtenerRamaMayor() {
        obtenerNumeroRamas(raiz, 0);
        return obtenerRamaMayor(raiz, 0, "", new ArrayList<String>());
    }

    private void obtenerNumeroRamas(NodoAVL pivote, int contador) {
        if (pivote != null) {
            contador++;
            obtenerNumeroRamas(pivote.getIzquierda(), contador);
            obtenerNumeroRamas(pivote.getDerecha(), contador);
            if (contador > numeroRamas) {
                numeroRamas = contador;
            }
        }
    }

    private ArrayList<String> obtenerRamaMayor(NodoAVL pivote, int contador, String dato, ArrayList lista) {

        if (pivote != null) {
            dato += pivote.getDato() + ",";
            contador++;
            lista = obtenerRamaMayor(pivote.getIzquierda(), contador, dato, lista);
            lista = obtenerRamaMayor(pivote.getDerecha(), contador, dato, lista);

            if (contador == numeroRamas) {
                lista.add(dato);
            }
        }

        return lista;
    }

    // buscar cualquier infante 
    public String buscar(String cedula) {
        boolean esta = buscar(this.raiz, cedula);
        if (esta) {
            return " si esta";
        } else {
            return " no esta";
        }
    }

    private boolean buscar(NodoAVL reco, String cedula) {
        if (reco == null) {
            return (false);
        }
        int compara = reco.getDato().getNroIdentificacion().compareTo(cedula);
        if (compara > 0) {
            return (buscar(reco.getIzquierda(), cedula));
        } else if (compara < 0) {
            return (buscar(reco.getDerecha(), cedula));
        } else {
            return (true);
        }
    }

    //altura del arbol
    int altura;

    public int retornarAltura() {
        altura = 0;
        retornarAltura(raiz, 1);
        return altura;
    }

    private void retornarAltura(NodoAVL reco, int nivel) {
        if (reco != null) {
            retornarAltura(reco.getIzquierda(), nivel + 1);
            if (nivel > altura) {
                altura = nivel;
            }
            retornarAltura(reco.getDerecha(), nivel + 1);
        }
    }

    //con nivel
    public ArrayList impNiveles() {
        ArrayList l = new ArrayList();
        impNiveles(raiz, 1, l);
        return l;
    }

    private void impNiveles(NodoAVL reco, int nivel, ArrayList l) {
        if (reco != null) {
            impNiveles(reco.getIzquierda(), nivel + 1, l);
            l.add(reco.getDato() + " Nivel: (" + nivel + ") ");
            impNiveles(reco.getDerecha(), nivel + 1, l);
        }
    }
}
