 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.modelo;

import com.arboles.excepciones.InfanteExcepcion;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Lucia Valencia
 */
public class ArbolN implements Serializable {

    private NodoN raiz;
    private int cantidadNodos;

    public ArbolN(NodoN raiz) {
        this.raiz = raiz;
    }

    public ArbolN() {
    }

    public NodoN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoN raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public void esVacio() throws InfanteExcepcion {
        if (raiz == null) {
            throw new InfanteExcepcion("El árbol está vacío");
        }
    }

    // metodo buscar infante por numero de identificacion
    public Infante buscarInfantexIdentificacion(String id) {
        return buscarInfantexIdentificacion(id, raiz);
    }

    private Infante buscarInfantexIdentificacion(String id, NodoN pivote) {
        if (pivote.getDato().getNroIdentificacion().compareTo(id) == 0) {
            return pivote.getDato();
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                Infante infanteBuscado = buscarInfantexIdentificacion(id, hijo);
                if (infanteBuscado != null) {
                    return infanteBuscado;
                }
            }
        }
        return null;
    }

    // metodo buscar padre de infantes 
    public NodoN buscarPadrexIdentificacion(String id){
        if (raiz.getDato().getNroIdentificacion().compareTo(id) == 0) {
            return null;
        }
        return buscarPadrexIdentificacion(id, raiz);
    }

    private NodoN buscarPadrexIdentificacion(String id, NodoN pivote){
        for (NodoN hijo : pivote.getHijos()) {
            if (hijo.getDato().getNroIdentificacion().compareTo(id) == 0) {
                return pivote;
            } else {
                NodoN padreBuscado = buscarPadrexIdentificacion(id, hijo);
                if (padreBuscado != null) {
                    return padreBuscado;
                }
            }
        }
        return null;
    }

    // metodo eliminar infantes, primero se busca el padre de infante a eliinar luego se preguta si tiene hijos 
    public boolean eliminarNodo(String id) throws InfanteExcepcion {
        NodoN padre = buscarPadrexIdentificacion(id);
        if (padre != null) {
            int cont = 0;
            for (NodoN hijo : padre.getHijos()) {
                if (hijo.getDato().getNroIdentificacion().compareTo(id) == 0) {
                    padre.getHijos().addAll(hijo.getHijos());
                    break;
                }
                cont++;
            }
            //verificar metodo equals
            padre.getHijos().remove(cont); //por posicion
            return true;
        }
        return false;
    }

    // buscar infante por identificacion
    public NodoN buscarInfantexId(String id) {
        if (raiz != null) {
            return buscarInfantexId(id, raiz);
        }
        return null;
    }

    private NodoN buscarInfantexId(String id, NodoN pivote) {
        if (pivote.getDato().getNroIdentificacion().compareTo(id) == 0) {
            return pivote;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                NodoN padreEncontrado = buscarInfantexId(id, hijo);
                if (padreEncontrado != null) {
                    return padreEncontrado;
                }
            }
        }
        return null;
    }

    // Adicionar nodo
    public void adicionarN(Infante dato, String padre) throws InfanteExcepcion {
        if (raiz == null) {
            raiz = new NodoN(dato);
        } else {
            NodoN padreEncontrado = buscarInfantexId(padre);
            if (padreEncontrado != null) {
                padreEncontrado.getHijos().add(new NodoN(dato));
            }

        }
        cantidadNodos++;
    }

    public void adicionarNodoN(Infante dato, Infante padre, int posicion) throws InfanteExcepcion {
        if (raiz == null) {
            raiz = new NodoN(dato);

        } else {
            adicionarNodoN(dato, padre, raiz, posicion);

        }
        cantidadNodos++;
    }

    private boolean adicionarNodoN(Infante dato, Infante padre, NodoN pivote, int posicion) throws InfanteExcepcion {
        boolean seAdiciona = false;
        if (pivote.getDato().getNroIdentificacion().compareTo(padre.getNroIdentificacion()) == 0) {
//            Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoN(dato));
            seAdiciona = true;
        } else {
            for (int i = 0; i < pivote.getHijos().size(); i++) {
                if (i == posicion) {

                    seAdiciona = adidicionarNodo(dato, padre, pivote.getHijos().get(i));

                    if (seAdiciona) {
                        break;
                    }
                }
            }
        }
        return seAdiciona;
    }

    public void adicionarNodo(Infante dato, Infante padre) throws InfanteExcepcion {
        if (raiz == null) {
            raiz = new NodoN(dato);
        } else {
            adidicionarNodo(dato, padre, raiz);
        }
        cantidadNodos++;
    }

    private boolean adidicionarNodo(Infante dato, Infante padre, NodoN pivote) throws InfanteExcepcion {
        boolean seAdiciona = false;
        if (pivote.getDato().getNroIdentificacion().compareTo(padre.getNroIdentificacion()) == 0) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoN(dato));
            seAdiciona = true;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                seAdiciona = adidicionarNodo(dato, padre, hijo);

                if (seAdiciona) {
                    break;
                }
            }
        }
        return seAdiciona;
    }

    public boolean intercambiarHijos(String idPadre1, String idPadre2) throws InfanteExcepcion {

        NodoN padre1 = buscarInfantexId(idPadre1);
        NodoN padre2 = buscarInfantexId(idPadre2);
        if (padre1 != null & padre2 != null) {
            if (padre1 != raiz & padre2 != raiz) {

                List<NodoN> hijosPadre1 = padre1.getHijos();
                padre1.setHijos(padre2.getHijos());
                padre2.setHijos(hijosPadre1);
            } else {
                throw new InfanteExcepcion("No se puede elegir la raiz");
            }
        } else {
            throw new InfanteExcepcion("NO EXISTE UN PADRE");
        }

        return false;

    }

    public void adicionarNodoxCodigo(Infante dato, Infante padre) throws InfanteExcepcion {
        if (raiz == null) {
            raiz = new NodoN(dato);

        } else {
            adicionarNodoxCodigo(dato, padre, raiz);

        }
        cantidadNodos++;

    }

    public boolean adicionarNodoxCodigo(Infante dato, Infante padre, NodoN pivote) throws InfanteExcepcion {
        // boolean adicionado=false;
        if (pivote.getDato().getCodigo() == padre.getCodigo()) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoN(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoN hijo : pivote.getHijos()) {

                if (adicionarNodoxCodigo(dato, padre, hijo)) {
                    break;
                }
            }
        }
        return false;
    }

}
