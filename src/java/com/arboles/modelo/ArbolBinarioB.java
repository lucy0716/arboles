/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arboles.modelo;

import com.arboles.excepciones.InfanteExcepcion;
import com.arboles.validadores.InfanteValidador;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author carloaiza
 */
public class ArbolBinarioB {

    private NodoABB raiz;
    private int cantidadNodos;

    public ArbolBinarioB() {
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public void adicionarNodo(Infante dato) throws InfanteExcepcion {
        InfanteValidador.verificarDatosObligatorios(dato);
        InfanteValidador.verificarEdadInfante(dato.getEdad());
        NodoABB nodo = new NodoABB(dato);
        if (raiz == null) {
            raiz = nodo;
            cantidadNodos++;
        } else {
            adicionarNodo(nodo, raiz);
            cantidadNodos++;
        }
    }

    private void adicionarNodo(NodoABB nuevo, NodoABB pivote) throws InfanteExcepcion {
        //Seleccionar el camino
        if (nuevo.getDato().getNroIdentificacion()
                .compareTo(pivote.getDato().getNroIdentificacion()) == 0) {
            throw new InfanteExcepcion("Ya existe un infante con la "
                    + "identificación " + nuevo.getDato().getNroIdentificacion());
        } else if (nuevo.getDato().getNroIdentificacion()
                .compareTo(pivote.getDato().getNroIdentificacion()) < 0) {
            ///Va para la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //va para la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }

    }

    public List<Infante> recorrerInOrden() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        List<Infante> listaInfantes = new ArrayList<>();
        recorrerInOrden(raiz, listaInfantes);
        return listaInfantes;
    }

    private void recorrerInOrden(NodoABB reco, List<Infante> listaInfantes) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listaInfantes);
            listaInfantes.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listaInfantes);
        }
    }

    public List<Infante> recorrerPreOrden() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        List<Infante> listaInfantes = new ArrayList<>();
        recorrerPreOrden(raiz, listaInfantes);
        return listaInfantes;
    }

    private void recorrerPreOrden(NodoABB reco, List<Infante> listaInfantes) {
        if (reco != null) {
            listaInfantes.add(reco.getDato());
            recorrerPreOrden(reco.getIzquierda(), listaInfantes);
            recorrerPreOrden(reco.getDerecha(), listaInfantes);
        }
    }

    public List<Infante> recorrerPostOrden() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        List<Infante> listaInfantes = new ArrayList<>();
        recorrerPostOrden(raiz, listaInfantes);
        return listaInfantes;
    }

    private void recorrerPostOrden(NodoABB reco, List<Infante> listaInfantes) {
        if (reco != null) {

            recorrerPostOrden(reco.getIzquierda(), listaInfantes);
            recorrerPostOrden(reco.getDerecha(), listaInfantes);
            listaInfantes.add(reco.getDato());
        }
    }

    public boolean esVacio() {
        return raiz == null;
    }

    //  Buscar menor Metodo parcial con coleeciones
    public byte buscarMenor() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        byte menor = 0;
        ArrayList listaInfantes = new ArrayList();
        buscarMenor(raiz, listaInfantes);
        menor = (byte) Collections.min(listaInfantes);

        return menor;
    }

    private void buscarMenor(NodoABB reco, ArrayList listaInfantes) {
        if (reco != null) {
            listaInfantes.add(reco.getDato().getEdad());
            buscarMenor(reco.getIzquierda(), listaInfantes);
            buscarMenor(reco.getDerecha(), listaInfantes);

        }
    }

    //buscar nodo MENOR por metodo recursivo
    public Infante buscarInfanteMenor() {
        return buscarInfanteMenor(raiz);
    }

    private Infante buscarInfanteMenor(NodoABB pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote.getDato());
    }

    //buscar nodo MAYOR metodo recursivo
    public Infante buscarInfanteMayor() {
        return buscarInfanteMayor(raiz);
    }

    private Infante buscarInfanteMayor(NodoABB pivote) {
        for (; pivote.getDerecha() != null; pivote = pivote.getDerecha());
        return (pivote.getDato());
    }

    // Sumar edades metodo normal
    private int sumaEdadesArbol;

    public int sumarEdadesArbol() {
        sumaEdadesArbol = 0;
        sumarInOrden(raiz);
        return sumaEdadesArbol;
    }

    private void sumarInOrden(NodoABB reco) {
        if (reco != null) {
            sumarInOrden(reco.getIzquierda());
            sumaEdadesArbol += reco.getDato().getEdad();
            sumarInOrden(reco.getDerecha());
        }

    }

    // Suma edades metodo recursivo
    public int sumarEdades() {
        return sumarEdades(raiz);
    }

    public int sumarEdades(NodoABB reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarEdades(reco.getIzquierda());
            suma += reco.getDato().getEdad();
            suma += sumarEdades(reco.getDerecha());
        }
        return suma;
    }

    public float calcularPromedioEdades() {
        return sumarEdades() / (float) cantidadNodos;
    }

    // Contar infantes que tengan la misma edad 
    public int contarInfantexEdad(byte edad) {
        return contarInfantexEdad(raiz, edad);
    }

    private int contarInfantexEdad(NodoABB reco, byte edad) {
        int cont = 0;
        if (reco != null) {
            if (reco.getDato().getEdad() == edad) {
                cont++;
            }
            cont += contarInfantexEdad(reco.getIzquierda(), edad);
            cont += contarInfantexEdad(reco.getIzquierda(), edad);
        }

        return cont;
    }

//    public int buscarHijos(byte edad){
//        return buscarHijos(raiz);
//    }
//    private int buscarHijos(NodoABB reco){
//        int cont=0;
//        if(reco !=null){
//            if(reco.getDato().getEdad()== edad){
//                cont++;
//            }
//            cont +=contarInfantexEdad(reco.getIzquierda(),edad);
//            cont +=contarInfantexEdad(reco.getIzquierda(),edad);
//        }
//        
//        return cont;
//    }
    // buscar infantes con edades iguales
    public int buscarEdadIguales(byte edad) {

        return buscarEdadIguales(raiz, edad);
    }

    private int buscarEdadIguales(NodoABB reco, byte edad) {
        int cont = 0;
        if (reco != null) {
            cont += buscarEdadIguales(reco.getIzquierda(), edad);
            if (edad == reco.getDato().getEdad()) {
                cont++;
            }
            cont += buscarEdadIguales(reco.getDerecha(), edad);
        }
        return cont;
    }

    // Listar hojas del arbol
    public List<Infante> listarHojas() throws InfanteExcepcion {
        if (esVacio()) {
            throw new InfanteExcepcion("El árbol está vació");
        }
        List<Infante> listaInfantes = new ArrayList<>();
        listarHojas(raiz, listaInfantes);

        return listaInfantes;
    }

    private void listarHojas(NodoABB reco, List<Infante> listaInfantes) {
        if (reco != null) {
            if (reco.getIzquierda() == null && reco.getDerecha() == null) {
                listaInfantes.add(reco.getDato());
            }
            listarHojas(reco.getIzquierda(), listaInfantes);
            listarHojas(reco.getDerecha(), listaInfantes);
        }
    }

    // Buscar infante por letra
    public List<Infante> buscarInfantexLetra(String letra) {
        List<Infante> listaInfante = new ArrayList<>();
        buscarInfantexLetra(raiz, listaInfante, letra);
        return listaInfante;
    }

    private void buscarInfantexLetra(NodoABB reco, List<Infante> infante, String letra) {

        if (reco != null) {
            if (reco.getDato().getNombre().toUpperCase().contains(letra.toUpperCase())) {
                infante.add(reco.getDato());
            }
            buscarInfantexLetra(reco.getIzquierda(), infante, letra);
            buscarInfantexLetra(reco.getDerecha(), infante, letra);
        }
    }

    // Buscar padre de un infante
    public List<Infante> buscarPadre(String identificacion) {
        List<Infante> listaInfantes = new ArrayList<>();
        buscarPadre(raiz, listaInfantes, identificacion);
        return listaInfantes;
    }

    private void buscarPadre(NodoABB reco, List<Infante> infante, String identificacion) {
        NodoABB padre = raiz;
        if (reco != null) {

            if (reco.getIzquierda() != null && (reco.getIzquierda().getDato().getNroIdentificacion().equals(identificacion))
                    || reco.getDerecha() != null && (reco.getDerecha().getDato().getNroIdentificacion().equals(identificacion))) {
                infante.add(reco.getDato());

            }
            buscarPadre(reco.getIzquierda(), infante, identificacion);
            buscarPadre(reco.getDerecha(), infante, identificacion);
        }
    }

    //Metodo Borrar el que sea
    private NodoABB buscarNodoMenor(NodoABB pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
    }

    public String borrarInfante(String identificacion) {
//        if (!this.buscar(x)) {
//            return 0;
//        }

        NodoABB z = borrarinfante(this.raiz, identificacion);
        this.setRaiz(z);
        return identificacion;
    }

    private NodoABB borrarinfante(NodoABB pivote, String identificacion) {
        if (pivote == null) {
            return null;//<--Dato no encontrado		
        }
        int compara = pivote.getDato().getNroIdentificacion().compareTo(identificacion);
        if (compara > 0) {
            pivote.setIzquierda(borrarinfante(pivote.getIzquierda(), identificacion));
        } else if (compara < 0) {
            pivote.setDerecha(borrarinfante(pivote.getDerecha(), identificacion));
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoABB cambiar = buscarNodoMenor(pivote.getDerecha());
                Infante aux = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(aux);
                pivote.setDerecha(borrarinfante(pivote.getDerecha(), identificacion));
            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda() : pivote.getDerecha();
            }
        }
        return pivote;
    }

    //metodo Podar arbol
    public void borarHojas() {
        borrarHojas(raiz);
    }

    private void borrarHojas(NodoABB pivote) {
        if (pivote != null) {
            if ((pivote.getIzquierda() == null && pivote.getDerecha() == null)) {
                borrarInfante(pivote.getDato().getNroIdentificacion());
            }
            borrarHojas(pivote.getIzquierda());
            borrarHojas(pivote.getDerecha());
        }
    }

    //Borrar infante de maayor edad
    public void borrarInfanteMayor() {
        borrarInfanteMayor(raiz, buscarInfanteMayor());
    }

    private void borrarInfanteMayor(NodoABB pivote, Infante infante) {
        if (pivote != null) {
            for (; pivote.getDerecha() != null; pivote = pivote.getDerecha());
            borrarInfante(pivote.getDato().getNroIdentificacion());
        }
    }

    //metodo borrar infante de menor edad
    public void borrarInfanteMenor() {
        borrarInfanteMenor(raiz, buscarInfanteMayor());
    }

    private void borrarInfanteMenor(NodoABB pivote, Infante infante) {
        if (pivote != null) {
            for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
            borrarInfante(pivote.getDato().getNroIdentificacion());
        }
    }

    // metodo de cambiar valores a los nodos
    public boolean cambiarValores() {
        cambiarValores(raiz);
        return true;
    }

    private void cambiarValores(NodoABB reco) {
        if (reco != null) {
            int valor = Integer.parseInt(reco.getDato().getNroIdentificacion());
            valor = valor * 3;
            String nuevoValor = String.valueOf(valor);
            reco.getDato().setNroIdentificacion(nuevoValor);
            cambiarValores(reco.getIzquierda());
            cambiarValores(reco.getDerecha());
        }
    }

    int numeroRamas = 0;

    public ArrayList<String> obtenerRamaMayor() {
        obtenerNumeroRamas(raiz, 0);
        return obtenerRamaMayor(raiz, 0, "", new ArrayList<String>());
    }

    private void obtenerNumeroRamas(NodoABB pivote, int contador) {
        if (pivote != null) {
            contador++;
            obtenerNumeroRamas(pivote.getIzquierda(), contador);
            obtenerNumeroRamas(pivote.getDerecha(), contador);
            if (contador > numeroRamas) {
                numeroRamas = contador;
            }
        }

    }

    private ArrayList<String> obtenerRamaMayor(NodoABB pivote, int contador, String dato, ArrayList lista) {

        if (pivote != null) {
            dato += pivote.getDato() + ",";
            contador++;
            lista = obtenerRamaMayor(pivote.getIzquierda(), contador, dato, lista);
            lista = obtenerRamaMayor(pivote.getDerecha(), contador, dato, lista);

            if (contador == numeroRamas) {
                lista.add(dato);
            }
        }

        return lista;
    }
}
