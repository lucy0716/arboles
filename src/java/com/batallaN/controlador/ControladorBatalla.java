/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallaN.controlador;

import com.arboles.utilidades.JsfUtil;
import com.batallaN.excepcion.BarcoExcepcion;
import com.batallaN.modelo.ArbolTipoBarco;
import com.batallaN.modelo.ArbolBarcoPosicionado;
import com.batallaN.modelo.BarcoPosicionado;
import com.batallaN.modelo.Coordenada;
import com.batallaN.modelo.NodoTipoBarco;
import com.batallaN.modelo.NodoBarcoPosicionado;
import com.batallaN.modelo.Rol;
import com.batallaN.modelo.TipoBarco;
import com.batallaN.modelo.Usuario;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author Lucia Valencia
 */
@Named(value = "controladorBatalla")
@ApplicationScoped
public class ControladorBatalla {

    //login 
    private String user;
    private String password;
    private Rol rolJuga;
    private Rol rolAdmin;
    private Usuario administrador;
    private Usuario jugador1;
    private Usuario jugador2;

    //gestionar barcos
    private ArbolBarcoPosicionado arbolBarcoPosicionadoJ1;
    private ArbolBarcoPosicionado arbolBarcoPosicionadoJ2;
    private TipoBarco tipoBarco = new TipoBarco();
    private ArbolTipoBarco arbolTipoBarco = new ArbolTipoBarco();
    private byte buscarTipoBarcoxCasilla;
    private DefaultDiagramModel modelTipoBarco;
    private DefaultDiagramModel modelBarcoPosicionado;
    private Usuario nuevoJugador = new Usuario();
    private List<BarcoPosicionado> listadoBarcoPosicionado;
    private BarcoPosicionado barcoAposicionarJ1 = new BarcoPosicionado();
    private final String[] orientaciones = new String[2];
    private String orientacionBarcoPosicionadoJ1;
    private byte posicionEnXJ1 = 1;
    private byte posicionEnYJ1 = 1;
    private String idBarcoJ1;
    private List<BarcoPosicionado> listadoBarcoPosicionadojuga2;
    private BarcoPosicionado barcoAposicionarJ2 = new BarcoPosicionado();
    private String orientacionBarcoPosicionadoJ2;
    private byte posicionEnXJ2 = 1;
    private byte posicionEnYJ2 = 1;
    private String idBarcoJ2;
    private String estado;
    private int enUndidosJ1 = 0;
    private int enUndidosJ2 = 0;
    private ArrayList<Coordenada> disparosJ1 = new ArrayList<>();
    private ArrayList<Coordenada> disparosJ2 = new ArrayList<>();

    private boolean enNuevo;
    private boolean enPosicionar;
    private boolean enPosicionarJuga2;
    private boolean turnoJ1 = true;

    public ControladorBatalla() {
    }

    public ArrayList<Coordenada> getDisparosJ1() {
        return disparosJ1;
    }

    // diparo
    public void setDisparosJ1(ArrayList<Coordenada> disparosJ1) {
        this.disparosJ1 = disparosJ1;
    }

    public ArrayList<Coordenada> getDisparosJ2() {
        return disparosJ2;
    }

    public void setDisparosJ2(ArrayList<Coordenada> disparosJ2) {
        this.disparosJ2 = disparosJ2;
    }

    public int getEnUndidosJ1() {
        return enUndidosJ1;
    }

    public void setEnUndidosJ1(int enUndidosJ1) {
        this.enUndidosJ1 = enUndidosJ1;
    }

    public int getEnUndidosJ2() {
        return enUndidosJ2;
    }

    public void setEnUndidosJ2(int enUndidosJ2) {
        this.enUndidosJ2 = enUndidosJ2;
    }

    public List<BarcoPosicionado> getListadoBarcoPosicionadojuga2() {
        return listadoBarcoPosicionadojuga2;
    }

    public void setListadoBarcoPosicionadojuga2(List<BarcoPosicionado> listadoBarcoPosicionadojuga2) {
        this.listadoBarcoPosicionadojuga2 = listadoBarcoPosicionadojuga2;
    }

    public BarcoPosicionado getBarcoAposicionarJ2() {
        return barcoAposicionarJ2;
    }

    public void setBarcoAposicionarJ2(BarcoPosicionado barcoAposicionarJ2) {
        this.barcoAposicionarJ2 = barcoAposicionarJ2;
    }

    public String getOrientacionBarcoPosicionadoJ2() {
        return orientacionBarcoPosicionadoJ2;
    }

    public void setOrientacionBarcoPosicionadoJ2(String orientacionBarcoPosicionadoJ2) {
        this.orientacionBarcoPosicionadoJ2 = orientacionBarcoPosicionadoJ2;
    }

    public byte getPosicionEnXJ2() {
        return posicionEnXJ2;
    }

    public void setPosicionEnXJ2(byte posicionEnXJ2) {
        this.posicionEnXJ2 = posicionEnXJ2;
    }

    public byte getPosicionEnYJ2() {
        return posicionEnYJ2;
    }

    public void setPosicionEnYJ2(byte posicionEnYJ2) {
        this.posicionEnYJ2 = posicionEnYJ2;
    }

    public String getIdBarcoJ2() {
        return idBarcoJ2;
    }

    public void setIdBarcoJ2(String idBarcoJ2) {
        this.idBarcoJ2 = idBarcoJ2;
    }

    public ArbolBarcoPosicionado getArbolBarcoPosicionadoJ2() {
        return arbolBarcoPosicionadoJ2;
    }

    public void setArbolBarcoPosicionadoJ2(ArbolBarcoPosicionado arbolBarcoPosicionadoJ2) {
        this.arbolBarcoPosicionadoJ2 = arbolBarcoPosicionadoJ2;
    }

    public boolean isTurnoJ1() {
        return turnoJ1;
    }

    public void setTurnoJ1(boolean turnoJ1) {
        this.turnoJ1 = turnoJ1;
    }

    public boolean isEnPosicionarJuga2() {
        return enPosicionarJuga2;
    }

    public void setEnPosicionarJuga2(boolean enPosicionarJuga2) {
        this.enPosicionarJuga2 = enPosicionarJuga2;
    }

    public String getIdBarcoJ1() {
        return idBarcoJ1;
    }

    public void setIdBarcoJ1(String idBarcoJ1) {
        this.idBarcoJ1 = idBarcoJ1;
    }

    public String getOrientacionBarcoPosicionadoJ1() {
        return orientacionBarcoPosicionadoJ1;
    }

    public void setOrientacionBarcoPosicionadoJ1(String orientacionBarcoPosicionadoJ1) {
        this.orientacionBarcoPosicionadoJ1 = orientacionBarcoPosicionadoJ1;
    }

    public byte getPosicionEnXJ1() {
        return posicionEnXJ1;
    }

    public void setPosicionEnXJ1(byte posicionEnXJ1) {
        this.posicionEnXJ1 = posicionEnXJ1;
    }

    public byte getPosicionEnYJ1() {
        return posicionEnYJ1;
    }

    public void setPosicionEnYJ1(byte posicionEnYJ1) {
        this.posicionEnYJ1 = posicionEnYJ1;
    }

    public String[] getOrientaciones() {
        return orientaciones;
    }

    public BarcoPosicionado getBarcoAposicionarJ1() {
        return barcoAposicionarJ1;
    }

    public void setBarcoAposicionarJ1(BarcoPosicionado barcoAposicionarJ1) {
        this.barcoAposicionarJ1 = barcoAposicionarJ1;
    }

    public ArbolBarcoPosicionado getArbolBarcoPosicionadoJ1() {
        return arbolBarcoPosicionadoJ1;
    }

    public void setArbolBarcoPosicionadoJ1(ArbolBarcoPosicionado arbolBarcoPosicionadoJ1) {
        this.arbolBarcoPosicionadoJ1 = arbolBarcoPosicionadoJ1;
    }

    public List<BarcoPosicionado> getListadoBarcoPosicionado() {
        return listadoBarcoPosicionado;
    }

    public void setListadoBarcoPosicionado(List<BarcoPosicionado> listadoBarcoPosicionado) {
        this.listadoBarcoPosicionado = listadoBarcoPosicionado;
    }

    public boolean isEnPosicionar() {
        return enPosicionar;
    }

    public void setEnPosicionar(boolean enPosicionar) {
        this.enPosicionar = enPosicionar;
    }

    public Usuario getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Usuario administrador) {
        this.administrador = administrador;
    }

    public Usuario getJugador1() {
        return jugador1;
    }

    public void setJugador1(Usuario jugador1) {
        this.jugador1 = jugador1;
    }

    public Usuario getJugador2() {
        return jugador2;
    }

    public void setJugador2(Usuario jugador2) {
        this.jugador2 = jugador2;
    }

    public Usuario getNuevoJugador() {
        return nuevoJugador;
    }

    public void setNuevoJugador(Usuario nuevoJugador) {
        this.nuevoJugador = nuevoJugador;
    }

    public byte getBuscarTipoBarcoxCasilla() {
        return buscarTipoBarcoxCasilla;
    }

    public void setBuscarTipoBarcoxCasilla(byte buscarTipoBarcoxCasilla) {
        this.buscarTipoBarcoxCasilla = buscarTipoBarcoxCasilla;
    }

    public boolean isEnNuevo() {
        return enNuevo;
    }

    public void setEnNuevo(boolean enNuevo) {
        this.enNuevo = enNuevo;
    }

    public TipoBarco getTipoBarco() {
        return tipoBarco;
    }

    public void setTipoBarco(TipoBarco tipoBarco) {
        this.tipoBarco = tipoBarco;
    }

    public ArbolTipoBarco getArbolTipoBarco() {
        return arbolTipoBarco;
    }

    public void setArbolTipoBarco(ArbolTipoBarco arbolTipoBarco) {
        this.arbolTipoBarco = arbolTipoBarco;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Rol getRolJuga() {
        return rolJuga;
    }

    public void setRolJuga(Rol rolJuga) {
        this.rolJuga = rolJuga;
    }

    public Rol getRolAdmin() {
        return rolAdmin;
    }

    public void setRolAdmin(Rol rolAdmin) {
        this.rolAdmin = rolAdmin;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @PostConstruct
    public void inicializar() {

        rolAdmin = new Rol((byte) 0, "Administrador");
        rolJuga = new Rol((byte) 1, "Jugador");
        administrador = new Usuario("admin", "admin", rolAdmin);
        jugador1 = new Usuario("111", "111", rolJuga);
        jugador2 = new Usuario("222", "222", rolJuga);
        enNuevo = false;

        orientaciones[0] = "Vertical";
        orientaciones[1] = "Horizontal";

        try {
            arbolTipoBarco.adicionarNodo(new TipoBarco("Buque", (byte) 4, (byte) 1));
            arbolTipoBarco.adicionarNodo(new TipoBarco("Velero", (byte) 3, (byte) 1));
//            arbolTipoBarco.adicionarNodo(new TipoBarco("Fragata", (byte) 5, (byte) 2));
//            arbolTipoBarco.adicionarNodo(new TipoBarco("Mi Barquito", (byte) 2, (byte) 1));
//            arbolTipoBarco.adicionarNodo(new TipoBarco("Mi Barco", (byte) 6, (byte) 1));

        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
            System.out.println("inicializar BarcoExcepcion " + ex.getMessage());
        }

        pintarArbolABB();

        try {
            cargarArbolNJ1();
            cargarArbolNJ2();
        } catch (Exception ex) {
            System.out.println("CargarArbolN Exception " + ex.getMessage());
        }
        try {
            pintarArbolN();
        } catch (Exception ex) {
            System.out.println("pintarArbolN Exception " + ex.getMessage());
        }

        System.out.println("Cantidad Naves " + arbolTipoBarco.contarBarcosPosicionados());
    }

    public String validadorLogin() throws BarcoExcepcion {

        if (administrador.getUser().compareTo(user) == 0 && administrador.getPassword().compareTo(password) == 0) {
            return "admin";

        } else if (jugador1.getUser().compareTo(user) == 0 && jugador1.getPassword().compareTo(password) == 0) {
            return "jugador1";
        } else if (jugador2.getUser().compareTo(user) == 0 && jugador2.getPassword().compareTo(password) == 0) {
            return "jugador2";
        }
        JsfUtil.addErrorMessage("Error en la Utentificacion");
        return "Error en la Utentificacion";
    }

    //______________BATALLA   NAVAL____________
    public void habilitarposicionar() {
        enPosicionar = !enPosicionar;
    }

    public void habilitarposicionarJuga2() {
        enPosicionarJuga2 = !enPosicionarJuga2;
    }

    //Arbol Binario 
    //habilitar var registrar
    public void habilitarVerRegistrar() {
        enNuevo = true;
    }

    //Cancelar Guardar 
    public void cancelarGuardar() {
        enNuevo = false;
    }

    // guardar arbolTipoBarco  boton
    public void guardarNave() {
        try {
            arbolTipoBarco.adicionarNodo(tipoBarco);
            tipoBarco = new TipoBarco();
            enNuevo = false;
            pintarArbolABB();
        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    //eliminar arbolTipoBarco
    public void eliminarNave() {
        arbolTipoBarco.borrarNave(buscarTipoBarcoxCasilla);
    }

    //adicionarJugador
    public void guardarJugador() {
        try {
            arbolTipoBarco.adicionarJugador(nuevoJugador);
            nuevoJugador = new Usuario();
            enNuevo = false;

        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }

    ////______________________ jugador 1_____________
    //Arbol N_Ario del jugador 1
    public void cargarArbolNJ1() {
        try {
            //Algoritmo
            arbolBarcoPosicionadoJ1 = new ArbolBarcoPosicionado();
            arbolBarcoPosicionadoJ1.adicionarNodo(new BarcoPosicionado("yo", new TipoBarco("yo", (byte) 0, (byte) 0)), null);
//            arbolBarcoPosicionadoJ1.adicionarNxCodigo(new BarcoPosicionado("yoxx", new TipoBarco("xxx", (byte) 0, (byte) 0), null), "yo");
            List<String> padres = new ArrayList<>();
            System.out.println("cargarArbolN");
            padres.add(arbolBarcoPosicionadoJ1.getRaiz().getDatos().getId());
            System.out.println(arbolBarcoPosicionadoJ1.getRaiz().getDatos().getTipoBarco().getNombre());
            ControladorBatalla.this.cargarArbolNJ1(arbolTipoBarco.getRaiz(), padres);
        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
            System.out.println("cargarArbolN BarcoExcepcion " + ex.getMessage());
        }
    }

    private void cargarArbolNJ1(NodoTipoBarco reco, List<String> padres) throws BarcoExcepcion {
        if (reco != null) {
            List<String> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidadJuego(); i++) {
                String idpapa = reco.getDato().getNombre() + "-" + i;
                BarcoPosicionado barcoNuevo = new BarcoPosicionado(idpapa, reco.getDato());
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolBarcoPosicionadoJ1.adicionarNxCodigo(barcoNuevo, padres.get(contPapas));
                padresNuevos.add(idpapa);
                contPapas++;
            }
            ControladorBatalla.this.cargarArbolNJ1(reco.getIzquierda(), padresNuevos);
            ControladorBatalla.this.cargarArbolNJ1(reco.getDerecha(), padresNuevos);
        }
    }

    //Arbol N_Ario del jugador dos 
    public void cargarArbolNJ2() {
        try {
            //Algoritmo
            arbolBarcoPosicionadoJ2 = new ArbolBarcoPosicionado();
            arbolBarcoPosicionadoJ2.adicionarNodo(new BarcoPosicionado("yo", new TipoBarco("yo", (byte) 0, (byte) 0)), null);
//            arbolBarcoPosicionadoJ1.adicionarNxCodigo(new BarcoPosicionado("yoxx", new TipoBarco("xxx", (byte) 0, (byte) 0), null), "yo");
            List<String> padres = new ArrayList<>();
            System.out.println("cargarArbolNjuga2");
            padres.add(arbolBarcoPosicionadoJ2.getRaiz().getDatos().getId());
            System.out.println(arbolBarcoPosicionadoJ2.getRaiz().getDatos().getTipoBarco().getNombre());
            ControladorBatalla.this.cargarArbolNJ2(arbolTipoBarco.getRaiz(), padres);
        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
            System.out.println("cargarArbolNjuga2 BarcoExcepcion " + ex.getMessage());
        }
    }

    private void cargarArbolNJ2(NodoTipoBarco reco, List<String> padres) throws BarcoExcepcion {
        if (reco != null) {
            List<String> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidadJuego(); i++) {
                String idpapa = reco.getDato().getNombre() + "-" + i;
                BarcoPosicionado barcoNuevo = new BarcoPosicionado(idpapa, reco.getDato());
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolBarcoPosicionadoJ2.adicionarNxCodigo(barcoNuevo, padres.get(contPapas));
                padresNuevos.add(idpapa);
                contPapas++;
            }
            ControladorBatalla.this.cargarArbolNJ2(reco.getIzquierda(), padresNuevos);
            ControladorBatalla.this.cargarArbolNJ2(reco.getDerecha(), padresNuevos);
        }
    }

    // obtener tamaño de tableros y generar  los tableros
    public int obtenerTamanoTablero() {
        int tamanoTablero = 0;
        int numBarcosPosicionados = arbolTipoBarco.contarBarcosPosicionados();
        if (numBarcosPosicionados <= 9) {
            tamanoTablero = 10;
        } else if (numBarcosPosicionados <= 20) {
            tamanoTablero = 20;
        } else {
            tamanoTablero = 30;
        }
        return tamanoTablero;
    }

    // verifica que las coordenadas noe excedan el tamaño del tablero, que no qeuden por fuera, ni cortas dichas coordenadas
    private void verificarPosicionTablero(BarcoPosicionado barco, int posX, int posY, String orientacion) throws BarcoExcepcion {
        if (posX > obtenerTamanoTablero() || posY > obtenerTamanoTablero()) {
            throw new BarcoExcepcion("La posición en X y Y ocaciona que la nave quede por fuera del tablero");
        }
        if (orientacion.equals(orientaciones[0])) { //Vertical
            if (posY + barco.getTipoBarco().getNumeroCasillas() > obtenerTamanoTablero() + 1) {
                throw new BarcoExcepcion("La posición en Y ocaciona que el Barco quede por fuera del tablero");
            }
        } else { // horizontal
            if (posX + barco.getTipoBarco().getNumeroCasillas() > obtenerTamanoTablero() + 1) {
                throw new BarcoExcepcion("La posición en x ocaciona que el Barco quede por fuera del tablero");
            }
        }
    }

    //  metodo de sobreposicion de  barcos Jugador1
    private void verificarSobrePosicionBarcoJ1(ArrayList<Coordenada> coordenadas) throws BarcoExcepcion {
        for (BarcoPosicionado barco : arbolBarcoPosicionadoJ1.listaBarcos()) {
            System.out.println("verificarSobrePosicionBarco() " + barco.getId());
            for (int i = 0; i < barco.getCoordenadas().size(); i++) {
                for (int j = 0; j < coordenadas.size(); j++) {
                    System.out.println("Coordenadas barco  " + barco.getCoordenadas().get(i) + " Nuevas coordenadas " + coordenadas.get(j));
                    if (barco.getCoordenadas().get(i).toString().equals(coordenadas.get(j).toString())) {
                        throw new BarcoExcepcion("El barco no se puede colocar sobre otro");
                    }
                }
            }
        }
    }

    private void verificarSobrePosicionBarcoJ2(ArrayList<Coordenada> coordenadas) throws BarcoExcepcion {
        for (BarcoPosicionado barco : arbolBarcoPosicionadoJ2.listaBarcos()) {
            System.out.println("verificarSobrePosicionBarco2() " + barco.getId());
            for (int i = 0; i < barco.getCoordenadas().size(); i++) {
                for (int j = 0; j < coordenadas.size(); j++) {
                    System.out.println("Coordenadas barco  " + barco.getCoordenadas().get(i) + " Nuevas coordenadas " + coordenadas.get(j));
                    if (barco.getCoordenadas().get(i).toString().equals(coordenadas.get(j).toString())) {
                        throw new BarcoExcepcion("El barco no se puede colocar sobre otro");
                    }
                }
            }
        }
    }

    // pintar barcos jugador 1
    public String pintarBarcosJ1(int fila, int columna) {
        for (BarcoPosicionado barco : arbolBarcoPosicionadoJ1.listaBarcos()) {
            for (Coordenada coord : barco.getCoordenadas()) {
                if (coord.getFila() == fila && coord.getColumna() == columna) {
                    return "width: 20px; height: 20px; background-color: green;";
                }
            }
        }
        return "width:20px; height: 20px; background-color: blue;";
    }

    // pintar barcos jugador 2
    public String pintarBarcosJ2(int fila, int columna) {
        for (BarcoPosicionado barco : arbolBarcoPosicionadoJ2.listaBarcos()) {
            for (Coordenada coord : barco.getCoordenadas()) {
                if (coord.getFila() == fila && coord.getColumna() == columna) {
                    return "width: 20px; height: 20px; background-color: green;";
                }
            }
        }
        return "width:20px; height: 20px; background-color: blue;";
    }

    // posicionar barcos , agregar coordenadas jugador uno
    public void posicionarBarcoJ1() {
//        System.out.println("posicionarBarcoJ1 " + idBarcoJ1);
////        System.out.println("posicionarBarcoJ1 x:" + posicionEnXJ1
//                + " y: " + posicionEnYJ1 + ", o: " + orientacionBarcoPosicionadoJ1);
        barcoAposicionarJ1 = arbolBarcoPosicionadoJ1.buscarBarcoPosicionadoxId(idBarcoJ1);
        try {
            verificarPosicionTablero(barcoAposicionarJ1, posicionEnXJ1, posicionEnYJ1, orientacionBarcoPosicionadoJ1);
            ArrayList<Coordenada> nuevasCoordenadas = new ArrayList<>();
            if (orientacionBarcoPosicionadoJ1.equals(orientaciones[0])) { //Vertical
                for (int i = 0; i < barcoAposicionarJ1.getTipoBarco().getNumeroCasillas(); i++) {
                    nuevasCoordenadas.add(new Coordenada((byte) (posicionEnYJ1 + i), posicionEnXJ1));
                }
            } else {
                for (int i = 0; i < barcoAposicionarJ1.getTipoBarco().getNumeroCasillas(); i++) {
                    nuevasCoordenadas.add(new Coordenada(posicionEnYJ1, (byte) (posicionEnXJ1 + i)));
                }
            }
//            for (Coordenada nuevasCoordenada : nuevasCoordenadas) {
//                System.out.println("nuevasCoordenadas  " + nuevasCoordenada);
//            }
            verificarSobrePosicionBarcoJ1(nuevasCoordenadas);
            barcoAposicionarJ1.setCoordenadas(nuevasCoordenadas);
            JsfUtil.addErrorMessage("Se ha posicionado exitosamente");
//            System.out.println("todo bien  ");
        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
            System.out.println("excepcion  " + ex.getMessage());
        }
    }

    // posicionar barcos , agregar coordenadas Jugador dos
    public void posicionarBarcoJ2() {
//        System.out.println("posicionarBarcoJ1 " + idBarcoJ2);
//        System.out.println("posicionarBarcoJ1 x:" + posicionEnXJ2
//                + " y: " + posicionEnYJ2 + ", o: " + orientacionBarcoPosicionadoJ2);
        barcoAposicionarJ2 = arbolBarcoPosicionadoJ2.buscarBarcoPosicionadoxId(idBarcoJ2);
        try {
            verificarPosicionTablero(barcoAposicionarJ2, posicionEnXJ2, posicionEnYJ2, orientacionBarcoPosicionadoJ2);
            ArrayList<Coordenada> nuevasCoordenadas = new ArrayList<>();
            if (orientacionBarcoPosicionadoJ2.equals(orientaciones[0])) { //Vertical
                for (int i = 0; i < barcoAposicionarJ2.getTipoBarco().getNumeroCasillas(); i++) {
                    nuevasCoordenadas.add(new Coordenada((byte) (posicionEnYJ2 + i), posicionEnXJ2));
                }
            } else {
                for (int i = 0; i < barcoAposicionarJ2.getTipoBarco().getNumeroCasillas(); i++) {
                    nuevasCoordenadas.add(new Coordenada(posicionEnYJ2, (byte) (posicionEnXJ2 + i)));
                }
            }
            for (Coordenada nuevasCoordenada : nuevasCoordenadas) {
//                System.out.println("nuevasCoordenadas  " + nuevasCoordenada);
            }
            verificarSobrePosicionBarcoJ2(nuevasCoordenadas);
            barcoAposicionarJ2.setCoordenadas(nuevasCoordenadas);
            System.out.println("todo bien  ");
        } catch (BarcoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
            System.out.println("excepcion  " + ex.getMessage());
        }
    }

    // habilitar juego batalla
    public boolean habilitarJugar() {
//        System.out.println("habilitar " + arbolBarcoPosicionadoJ1.listaBarcosNoPosicionados().isEmpty());
//        System.out.println("habilitar " + arbolBarcoPosicionadoJ2.listaBarcosNoPosicionados().isEmpty());
        return arbolBarcoPosicionadoJ1.listaBarcosNoPosicionados().isEmpty() && arbolBarcoPosicionadoJ2.listaBarcosNoPosicionados().isEmpty();
    }

    // barco con posicion jugador uno
    public BarcoPosicionado posicionConBarcoJ1(int fila, int columna) {
        for (BarcoPosicionado barco : arbolBarcoPosicionadoJ1.listaBarcos()) {
            System.out.println("posicionConBarcoJ1 coord " + barco.getCoordenadas() + " fila " + fila + " columna " + columna);
            for (Coordenada coord : barco.getCoordenadas()) {
                if (coord.getFila() == fila && coord.getColumna() == columna) {
                    return barco;
                }
            }
        }
        return null;
    }

    // barco con posicion jugador dos
    public BarcoPosicionado posicionConBarcoJ2(int fila, int columna) {
        for (BarcoPosicionado barco : arbolBarcoPosicionadoJ2.listaBarcos()) {
            System.out.println("posicionConBarcoJ2 coord " + barco.getCoordenadas() + " fila " + fila + " columna " + columna);
            for (Coordenada coord : barco.getCoordenadas()) {
                if (coord.getFila() == fila && coord.getColumna() == columna) {
                    return barco;
                }
            }
        }
        return null;
    }

    // metodo disabled en el juego_ Disparo contraicante jugado uno 
    public boolean disparoContrincanteJ1(int fila, int columna) {
        for (Coordenada coord : disparosJ1) {
            if (coord.getFila() == fila && coord.getColumna() == columna) {
                return true;
            }
        }
        return false;
    }

    // metodo disabled en el juego_ Disparo contraicante jugado Dos
    public boolean disparoContrincanteJ2(int fila, int columna) {
        for (Coordenada coord : disparosJ2) {
            if (coord.getFila() == fila && coord.getColumna() == columna) {
                return true;
            }
        }
        return false;
    }

    public boolean existeGanador() {
        System.out.println("Ganador J1 " + enUndidosJ1 + " J2 " + enUndidosJ2 + " suma " + arbolTipoBarco.sumaCantidadTipo());

        if (enUndidosJ1 == arbolTipoBarco.sumaCantidadTipo()) {
            JsfUtil.addSuccessMessage("El ganador es el jugador 2");
        }
        if (enUndidosJ2 == arbolTipoBarco.sumaCantidadTipo()) {
            JsfUtil.addSuccessMessage("El ganador es el jugador 1");
        }

        return enUndidosJ1 == arbolTipoBarco.sumaCantidadTipo() || enUndidosJ2 == arbolTipoBarco.sumaCantidadTipo();
    }

    public void dispararContrincanteJ1(int fila, int columna) {
        System.out.println("----------------------------\n DcJ1 turno J1 " + turnoJ1);
        if (!existeGanador()) {
            if (turnoJ1) {
                disparosJ1.add(new Coordenada((byte) fila, (byte) columna));
                BarcoPosicionado barco = posicionConBarcoJ2(fila, columna);
                if (barco != null) {
                    System.out.println("DcJ1 barco " + barco.getId());
                    barco.adicionarImpactos();
                    if (barco.getEstado().equals("Hundido")) {
                        enUndidosJ2++;
                        JsfUtil.addSuccessMessage("Has derrivado la nave " + barco.getId());
                        System.out.println("DcJ1 Has derrivado la nave " + barco.getId());
                    } else {
                        JsfUtil.addSuccessMessage("Has dado a una nave " + barco.getId());
                        System.out.println("DcJ1 Has dado a una nave " + barco.getId());
                    }
                    turnoJ1 = true;
                } else {
                    System.out.println("DcJ1 barco null");
                    turnoJ1 = false;
                }
            }
        }

    }

    public void dispararContrincanteJ2(int fila, int columna) {
        System.out.println("----------------------------\nDcJ2 turno J1 " + turnoJ1);
        if (!turnoJ1) {
            if (!existeGanador()) {
                disparosJ2.add(new Coordenada((byte) fila, (byte) columna));
                BarcoPosicionado barco = posicionConBarcoJ1(fila, columna);
                if (barco != null) {
                    System.out.println("DcJ2 barco " + barco.getId());
                    barco.adicionarImpactos();
                    if (barco.getEstado().equals("Hundido")) {
                        enUndidosJ1++;
                        JsfUtil.addSuccessMessage("Has derrivado la nave " + barco.getId());
                        System.out.println("DcJ2 Has derrivado la nave " + barco.getId());
                    } else {
                        JsfUtil.addSuccessMessage("Has dado a una nave ");
                        System.out.println("DcJ1 Has dado a una nave " + barco.getId());
                    }
                    turnoJ1 = false;
                } else {
                    System.out.println("DcJ1 barco null");
                    turnoJ1 = true;
                }
            }
        }
    }

    //    public void pintarDiparo(int columna, int fila) {
    //        if (disparosJ1 != null) {
    //            for (Coordenada disparo : disparosJ1) {
    //                if (disparo.getFila() == fila && disparo.getColumna() == columna) {
    //                    for (BarcoPosicionado barco : arbolBarcoPosicionadoJ1.listaBarcos()) {
    //                    }
    //                }
    //            }
    //        }
    //    }
    // __________________TEMPORAL ____________________
    // pintar arbol naves binario
    public void pintarArbolABB() {

        modelTipoBarco = new DefaultDiagramModel();
        modelTipoBarco.setMaxConnections(-1);
        modelTipoBarco.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        modelTipoBarco.setDefaultConnector(connector);
        pintarArbolABB(arbolTipoBarco.getRaiz(), modelTipoBarco, null, 18, 0);
    }

    private void pintarArbolABB(NodoTipoBarco reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato().getNombre() + " - " + reco.getDato().getCantidadJuego());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);

            pintarArbolABB(reco.getIzquierda(), model, elementHijo, x - 5, y + 10);
            pintarArbolABB(reco.getDerecha(), model, elementHijo, x + 5, y + 10);
        }
    }

    private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");

        return endPoint;
    }

    public DefaultDiagramModel getModelTipoBarco() {
        return modelTipoBarco;
    }

    public void pintarArbolN() {
        modelBarcoPosicionado = new DefaultDiagramModel();
        modelBarcoPosicionado.setMaxConnections(-1);
        modelBarcoPosicionado.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        modelBarcoPosicionado.setDefaultConnector(connector);
        pintarArbolN(arbolBarcoPosicionadoJ2.getRaiz(), modelBarcoPosicionado, null, 30, 0);
    }

    private void pintarArbolN(NodoBarcoPosicionado reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDatos().getId());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            //elementHijo.setId(reco.getDato().getNroIdentificacion());

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);
            for (NodoBarcoPosicionado hijo : reco.getHijos()) {
                pintarArbolN(hijo, model, elementHijo, x - 10, y + 5);
                x += 10;
            }
        }
    }

    public DefaultDiagramModel getModelBarcoPosicionado() {
        return modelBarcoPosicionado;
    }
}
