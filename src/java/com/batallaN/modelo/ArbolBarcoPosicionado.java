/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallaN.modelo;

import com.batallaN.excepcion.BarcoExcepcion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucia Valencia
 */
public class ArbolBarcoPosicionado implements Serializable {

    private NodoBarcoPosicionado raiz;
    private int cantidadNodos;

    public ArbolBarcoPosicionado(NodoBarcoPosicionado raiz) {
        this.raiz = raiz;
    }

    public ArbolBarcoPosicionado() {
    }

    public NodoBarcoPosicionado getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBarcoPosicionado raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    //Adiccionar Nodo
    public void adicionarNodo(BarcoPosicionado dato, BarcoPosicionado padre) throws BarcoExcepcion {
        if (raiz == null) {
            raiz = new NodoBarcoPosicionado(dato);
        } else {
            adidicionarNodo(dato, padre, raiz);
        }
    }

    public boolean adidicionarNodo(BarcoPosicionado dato, BarcoPosicionado padre, NodoBarcoPosicionado pivote) throws BarcoExcepcion {
        boolean seAdiciona = false;
        if (pivote.getDatos().getTipoBarco().equals(padre.getTipoBarco())) {
            //Es el padre donde debo adicionar             
            pivote.getHijos().add(new NodoBarcoPosicionado(dato));
            seAdiciona = true;
        } else {
            for (NodoBarcoPosicionado hijo : pivote.getHijos()) {
                seAdiciona = adidicionarNodo(dato, padre, hijo);
                if (seAdiciona) {
                    break;
                }
            }
        }
        return seAdiciona;
    }

    //Adiccionar Nodo por identificador
    public void adicionarNxCodigo(BarcoPosicionado dato, String idpadre) throws BarcoExcepcion {
        if (raiz == null) {
            raiz = new NodoBarcoPosicionado(dato);
        } else {
            adicionarNxCodigo(dato, idpadre, raiz);
        }
        cantidadNodos++;
    }

    public boolean adicionarNxCodigo(BarcoPosicionado dato, String idpadre, NodoBarcoPosicionado pivote) throws BarcoExcepcion {
        if (pivote.getDatos().getId().equals(idpadre)) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoBarcoPosicionado(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoBarcoPosicionado hijo : pivote.getHijos()) {
                if (adicionarNxCodigo(dato, idpadre, hijo)) {
                    break;
                }
            }
        }
        return false;
    }

    // buscar hijo en baarcos n-arios
    public boolean buscaHijos(NodoBarcoPosicionado nodo) {
        if (nodo.getHijos() != null) {
            return true;
        } else {
            return false;
        }
    }

    // lista barcos
    public List<BarcoPosicionado> listaBarcos() {
        List<BarcoPosicionado> lista = new ArrayList<>();
        listaBarcos(raiz, lista);
        return lista;
    }

    private void listaBarcos(NodoBarcoPosicionado pivote, List<BarcoPosicionado> lista) {
        if (pivote != null) {
            if (buscaHijos(pivote)) {
                for (NodoBarcoPosicionado hijo : pivote.getHijos()) {
                    listaBarcos(hijo, lista);
                    lista.add(hijo.getDatos());
                }

            }
        }
    }

    public List<BarcoPosicionado> listaBarcosNoPosicionados() {
        List<BarcoPosicionado> lista = new ArrayList<>();
        listaBarcosNoPosicionados(raiz, lista);
        return lista;
    }

    private void listaBarcosNoPosicionados(NodoBarcoPosicionado pivote, List<BarcoPosicionado> lista) {
        if (pivote != null) {
            if (buscaHijos(pivote)) {
                for (NodoBarcoPosicionado hijo : pivote.getHijos()) {
                    listaBarcosNoPosicionados(hijo, lista);
                    System.out.println(hijo.getDatos());
                    if (hijo.getDatos().getCoordenadas().isEmpty()) {
                        lista.add(hijo.getDatos());
                    }
                }
            }
        }
    }

    // buscar barco posicionado por id 
    public BarcoPosicionado buscarBarcoPosicionadoxId(String id) {
        if (raiz != null) {
            return buscarBarcoPosicionadoxId(id, raiz);
        }
        return null;
    }

    private BarcoPosicionado buscarBarcoPosicionadoxId(String id, NodoBarcoPosicionado pivote) {
        if (pivote.getDatos().getId().compareTo(id) == 0) {
            return pivote.getDatos();
        } else {
            for (NodoBarcoPosicionado hijo : pivote.getHijos()) {
                BarcoPosicionado nodoEncontrado = buscarBarcoPosicionadoxId(id, hijo);
                if (nodoEncontrado != null) {
                    return nodoEncontrado;
                }
            }
            return null;
        }
    }
}
