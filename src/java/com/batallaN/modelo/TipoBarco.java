/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallaN.modelo;

/**
 *
 * @author Lucia Valencia
 */
public class TipoBarco {

    private String nombre;
    private byte numeroCasillas;
    private byte cantidadJuego;
    private int codigo;

    public TipoBarco() {
    }

    public TipoBarco(String nombre, byte numeroCasillas, byte cantidadJuego) {
        this.nombre = nombre;
        this.numeroCasillas = numeroCasillas;
        this.cantidadJuego = cantidadJuego;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte getNumeroCasillas() {
        return numeroCasillas;
    }

    public void setNumeroCasillas(byte numeroCasillas) {
        this.numeroCasillas = numeroCasillas;
    }

    public byte getCantidadJuego() {
        return cantidadJuego;
    }

    public void setCantidadJuego(byte cantidadJuego) {
        this.cantidadJuego = cantidadJuego;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
