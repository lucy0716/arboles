/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallaN.modelo;

import com.batallaN.excepcion.BarcoExcepcion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucia Valencia
 */
public class ArbolTipoBarco {

    private NodoTipoBarco raiz;
    private int cantidadNodos;

    public ArbolTipoBarco() {
    }

    public NodoTipoBarco getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoTipoBarco raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public boolean esVacio() {
        return raiz == null;
    }

    //Adicionar Nave
    public void adicionarNodo(TipoBarco dato) throws BarcoExcepcion {
        NodoTipoBarco nodo = new NodoTipoBarco(dato);
        if (raiz == null) {
            raiz = nodo;
            cantidadNodos++;
        } else {
            adicionarNodo(nodo, raiz);
            cantidadNodos++;
        }
    }

    private void adicionarNodo(NodoTipoBarco nuevo, NodoTipoBarco pivote) throws BarcoExcepcion {
        //Seleccionar el camino
        if (nuevo.getDato().getNombre().equals(pivote.getDato().getNombre())) {
            throw new BarcoExcepcion("Ya existe un barco con el nombre:  " + nuevo.getDato().getNombre());
        } else if (nuevo.getDato().getNumeroCasillas() == pivote.getDato().getNumeroCasillas()) {
            throw new BarcoExcepcion("Ya existe un barco con ese numero de casillas:  " + nuevo.getDato().getNombre());
        } else if (nuevo.getDato().getNumeroCasillas() < pivote.getDato().getNumeroCasillas()) {
            ///Va para la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);

            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //va para la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }

    }

    //Listar los barcos
    public List<TipoBarco> listar() throws BarcoExcepcion {
        if (esVacio()) {
            throw new BarcoExcepcion("El árbol está vació");
        }
        List<TipoBarco> listaInfantes = new ArrayList<>();
        listar(raiz, listaInfantes);
        return listaInfantes;
    }

    private void listar(NodoTipoBarco reco, List<TipoBarco> lista) {
        if (reco != null) {

            lista.add(reco.getDato());
            listar(reco.getDerecha(), lista);
            listar(reco.getIzquierda(), lista);

        }
    }

    // metodo de buscar para el metodo de eliminar
    private NodoTipoBarco buscarNaveMenor(NodoTipoBarco pivote) {
        for (; pivote.getIzquierda() != null; pivote = pivote.getIzquierda());
        return (pivote);
    }

    //Elimiar alguna de las naves
    public void borrarNave(byte numCasilla) {

        NodoTipoBarco z = borrarNave(this.raiz, numCasilla);
        this.setRaiz(z);

    }

    private NodoTipoBarco borrarNave(NodoTipoBarco pivote, byte numCasilla) {
        if (pivote == null) {
            return null;//<--Dato no encontrado		
        }

        if (numCasilla < pivote.getDato().getNumeroCasillas()) {
            pivote.setIzquierda(borrarNave(pivote.getIzquierda(), numCasilla));
        } else if (numCasilla > pivote.getDato().getNumeroCasillas()) {
            pivote.setDerecha(borrarNave(pivote.getDerecha(), numCasilla));
        } else {
            if (pivote.getIzquierda() != null && pivote.getDerecha() != null) {
                /*
                 *	Buscar el menor de los derechos y lo intercambia por el dato
                 *	que desea borrar. La idea del algoritmo es que el dato a borrar 
                 *	se coloque en una hoja o en un nodo que no tenga una de sus ramas.
                 **/
                NodoTipoBarco cambiar = buscarNaveMenor(pivote.getDerecha());
                TipoBarco aux = cambiar.getDato();
                cambiar.setDato(pivote.getDato());
                pivote.setDato(aux);
                pivote.setDerecha(borrarNave(pivote.getDerecha(),
                        numCasilla));

            } else {
                pivote = (pivote.getIzquierda() != null) ? pivote.getIzquierda()
                        : pivote.getDerecha();

            }
        }
        return pivote;
    }

    // recorrer inorden
    public List<TipoBarco> recorrerInOrden() {
        if (esVacio()) {
            return null;
        }
        List<TipoBarco> listaInfantes = new ArrayList<>();
        recorrerInOrden(raiz, listaInfantes);
        return listaInfantes;
    }

    private void recorrerInOrden(NodoTipoBarco reco, List<TipoBarco> listaInfantes) {
        if (reco != null) {
            recorrerInOrden(reco.getIzquierda(), listaInfantes);
            listaInfantes.add(reco.getDato());
            recorrerInOrden(reco.getDerecha(), listaInfantes);
        }
    }

    //Suma las naves existentes
    public int sumaCantidadTipo() {

        return sumaCantidadTipo(raiz);
    }

    private int sumaCantidadTipo(NodoTipoBarco reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumaCantidadTipo(reco.getIzquierda());
            suma += (reco.getDato().getCantidadJuego());
            suma += sumaCantidadTipo(reco.getDerecha());
        }
        return suma;
    }

    // Adicionar Jugador
    public List<Usuario> adicionarJugador(Usuario jugador) throws BarcoExcepcion {
        List<Usuario> jugadorN = new ArrayList<>();
        for (int i = 0; i < jugadorN.size(); i++) {
            if (jugadorN.get(i).getUser().equals(jugador.getUser())) {
                throw new BarcoExcepcion("El usuario ya esta registrado");
            } else {
                jugadorN.add(jugador);
            }
        }
        return jugadorN;
    }

    // obtener cantidad de las naves
    public int contarBarcosPosicionados() {
        int suma = 0;

        List<TipoBarco> lista = recorrerInOrden();
        List<Byte> listaNaves = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            suma += lista.get(i).getCantidadJuego();
        }

        return suma;

    }

}
