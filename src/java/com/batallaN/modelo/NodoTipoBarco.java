/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallaN.modelo;

/**
 *
 * @author Lucia Valencia
 */
public class NodoTipoBarco {

    private TipoBarco dato;
    private NodoTipoBarco izquierda;
    private NodoTipoBarco derecha;

    public NodoTipoBarco(TipoBarco dato) {
        this.dato = dato;
    }

    public TipoBarco getDato() {
        return dato;
    }

    public void setDato(TipoBarco dato) {
        this.dato = dato;
    }

    public NodoTipoBarco getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoTipoBarco izquierda) {
        this.izquierda = izquierda;
    }

    public NodoTipoBarco getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoTipoBarco derecha) {
        this.derecha = derecha;
    }

}
