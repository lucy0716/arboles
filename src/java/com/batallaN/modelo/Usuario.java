/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallaN.modelo;

/**
 *
 * @author Lucia Valencia
 */
public class Usuario {

    private String user;
    private String password;
    private Rol tipoRol;

    public Usuario() {
    }

    public Usuario(String user, String password, Rol tipoRol) {
        this.user = user;
        this.password = password;
        this.tipoRol = tipoRol;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Rol getTipoRol() {
        return tipoRol;
    }

    public void setTipoRol(Rol tipoRol) {
        this.tipoRol = tipoRol;
    }

}
