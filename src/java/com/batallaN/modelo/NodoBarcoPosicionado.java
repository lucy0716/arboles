/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallaN.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lucia Valencia
 */
public class NodoBarcoPosicionado implements Serializable{

    private BarcoPosicionado datos;
    private List<NodoBarcoPosicionado> hijos;

    public NodoBarcoPosicionado(BarcoPosicionado datos ) {
        this.datos = datos;
        hijos = new ArrayList<>();
       
    }

   
    public NodoBarcoPosicionado() {
    }
   
    public BarcoPosicionado getDatos() {
        return datos;
    }

    public void setDatos(BarcoPosicionado datos) {
        this.datos = datos;
    }

    public List<NodoBarcoPosicionado> getHijos() {
        return hijos;
    }

    public void setHijos(List<NodoBarcoPosicionado> hijos) {
        this.hijos = hijos;
    }

}
