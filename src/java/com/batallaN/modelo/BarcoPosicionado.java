/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallaN.modelo;

import java.util.ArrayList;

/**
 *
 * @author Lucia Valencia
 */
public class BarcoPosicionado {

    private TipoBarco tipoBarco;
    private ArrayList<Coordenada> coordenadas;
    private byte numeroImpactos;

    private String id;

    public BarcoPosicionado(String id, TipoBarco tipoBarco) {
        this.id = id;
        this.tipoBarco = tipoBarco;
        coordenadas = new ArrayList<>();
        numeroImpactos = 0;
    }

    public BarcoPosicionado() {
    }

    public TipoBarco getTipoBarco() {
        return tipoBarco;
    }

    public void setTipoBarco(TipoBarco tipoBarco) {
        this.tipoBarco = tipoBarco;
    }

    public ArrayList<Coordenada> getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(ArrayList<Coordenada> coordenadas) {
        this.coordenadas = coordenadas;
    }

    public byte getNumeroImpactos() {
        return numeroImpactos;
    }

    public void setNumeroImpactos(byte numeroImpactos) {
        this.numeroImpactos = numeroImpactos;
    }

    public String getEstado() {
        if (numeroImpactos == 0) {
            return "Ileso";
        } else if (numeroImpactos == tipoBarco.getNumeroCasillas()) {
            return "Hundido";
        } else {
            return "Tocado";
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void adicionarImpactos() {
        numeroImpactos = (byte) (numeroImpactos + 1);
    }

    @Override
    public String toString() {
        return "tipoBarco=" + tipoBarco
                + ", coordenadas=" + coordenadas + "(" + coordenadas.size() + ")"
                + ", numeroImpactos=" + numeroImpactos
                + ", estado=" + getEstado()
                + ", id=" + id;
    }
}
